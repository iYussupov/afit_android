package com.fxofficeapp.a_fit.Activities;

import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.TouchImageView;

public class ImageViewerActivity extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_image_viewer, container, false);

        View decorView = getActivity().getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        //Getting Image Uri
        String img_uri  = getArguments().getString("image_uri");
        if (img_uri != null) {

            TouchImageView postThumb = (TouchImageView)v.findViewById(R.id.image_viewer);
            postThumb.setImageUrl(img_uri);

        }

        return  v;
    }
}
