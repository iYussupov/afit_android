package com.fxofficeapp.a_fit.Activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Models.About;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.fxofficeapp.a_fit.Slideshow.SlideShowAdapter;
import com.fxofficeapp.a_fit.Slideshow.ViewPagerSlideShow;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class AboutUsActivity extends DrawerActivity {

    com.fxofficeapp.a_fit.Helper.CustomFont aboutTitle;
    com.fxofficeapp.a_fit.Helper.CustomTextView aboutContent;
    private About about;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ArrayList<String> imagesArray = new ArrayList<String>();
    String[] IMAGES;
    ScrollView contentFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        contentFrame = (ScrollView)findViewById(R.id.content_wrapper);
        contentFrame.setVisibility(View.GONE);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.about_us);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        aboutTitle = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.about_title);
        aboutContent = (com.fxofficeapp.a_fit.Helper.CustomTextView)findViewById(R.id.about_content);

        parseData();
        checkTimeStamp();

    }

    void checkTimeStamp() {
        String about_data = preferences.getString("about_data_"+Constants.URL_SLUG, "");
        if(!about_data.equalsIgnoreCase(""))
        {
            try {

                JSONObject obj = new JSONObject(about_data);
                final int timeStampCached = obj.getInt("time");

                // Instantiate the RequestQueue.
                String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_ABOUT;
                RequestQueue queue = Volley.newRequestQueue(this);


                Log.v("cache", "checking timestamp");

                // Request a string response from the provided URL.
                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                try {

                                    int timeStamp = response.getInt("time");

                                    if (timeStampCached != timeStamp) {

                                        parseFreshData();
                                        Log.v("cache", "refresh data");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });

                // Add the request to the RequestQueue.
                queue.add(jsObjRequest);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void parseData() {

        String about_data = preferences.getString("about_data_"+Constants.URL_SLUG, "");
        if(!about_data.equalsIgnoreCase(""))
        {

            Log.v("cache parse", "we are here");
            try {

                JSONObject obj = new JSONObject(about_data);
                updateModel(obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

            parseFreshData();

        }

    }

    void parseFreshData(){

        // Instantiate the RequestQueue.
        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_ABOUT;
        RequestQueue queue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
            (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    editor.putString("about_data_"+Constants.URL_SLUG,response.toString());
                    editor.commit();

                    updateModel(response);

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
                }
            });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);
    }

    void updateModel(JSONObject response){
        try {

            int timeStamp = response.getInt("time");
            String title = response.getString("club_title");
            String content = response.getString("club_description");
            JSONObject images = null;
            try {
                images = response.getJSONObject("club_image");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            about = new About(timeStamp, title, content, images);

            updateUI(about);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void updateUI(About about){
        imagesArray.clear();
        IMAGES = new String[0];

        aboutTitle.setAlpha(0);
        aboutContent.setAlpha(0);
        aboutTitle.setText(about.getTitle().toUpperCase());
        aboutContent.setText(about.getContent());

        if (about.getImageUrl() != null) {
            JSONObject images = about.getImageUrl();

            Iterator<String> iter = images.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    String img = (String) images.get(key);
                    imagesArray.add(img);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            IMAGES = new String[imagesArray.size()];
            imagesArray.toArray(IMAGES);

            ViewPagerSlideShow slideshow = (ViewPagerSlideShow) findViewById(R.id.slideshow);
            slideshow.setAlpha(0);

            DisplayMetrics dm = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(dm);
            Double size = dm.heightPixels * 0.4;
            slideshow.getLayoutParams().height = size.intValue();

            SlideShowAdapter adapter = new SlideShowAdapter(getSupportFragmentManager(), IMAGES);
            slideshow.setAdapter(adapter);
            ObjectAnimator SlideShowAnim = ObjectAnimator.ofPropertyValuesHolder(slideshow,
                    PropertyValuesHolder.ofFloat("alpha", 0f, 1.0f));
            SlideShowAnim.setDuration(300);
            SlideShowAnim.setStartDelay(700);
            SlideShowAnim.start();

        }


        contentFrame.setVisibility(View.VISIBLE);

        ObjectAnimator aboutContentAnim = ObjectAnimator.ofPropertyValuesHolder(aboutContent,
            PropertyValuesHolder.ofFloat("translationY", 150f, 0f),
            PropertyValuesHolder.ofFloat("alpha", 0f, 1.0f));
            aboutContentAnim.setInterpolator(new DecelerateInterpolator());
            aboutContentAnim.setDuration(400);
            aboutContentAnim.setStartDelay(700);
            aboutContentAnim.start();

        ObjectAnimator aboutTitleAnim = ObjectAnimator.ofPropertyValuesHolder(aboutTitle,
            PropertyValuesHolder.ofFloat("translationY", 150f, 0f),
            PropertyValuesHolder.ofFloat("alpha", 0f, 1.0f));
            aboutContentAnim.setInterpolator(new DecelerateInterpolator());
            aboutTitleAnim.setDuration(400);
            aboutTitleAnim.setStartDelay(600);
            aboutTitleAnim.start();

    }

}
