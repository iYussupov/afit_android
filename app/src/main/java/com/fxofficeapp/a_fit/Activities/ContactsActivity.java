package com.fxofficeapp.a_fit.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Models.Contact;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContactsActivity extends DrawerActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private Contact contact;
    com.fxofficeapp.a_fit.Helper.CustomFont contactsTitle;
    com.fxofficeapp.a_fit.Helper.CustomTextView contactsAddress;
    com.fxofficeapp.a_fit.Helper.CustomFont workDay;
    com.fxofficeapp.a_fit.Helper.CustomFont weekEnd;
    com.fxofficeapp.a_fit.Helper.CustomFont depOneTitle;
    com.fxofficeapp.a_fit.Helper.CustomFont depOnePhoneOne;
    com.fxofficeapp.a_fit.Helper.CustomFont depOnePhoneTwo;
    com.fxofficeapp.a_fit.Helper.CustomFont depTwoTitle;
    com.fxofficeapp.a_fit.Helper.CustomFont depTwoPhoneOne;
    com.fxofficeapp.a_fit.Helper.CustomFont depTwoPhoneTwo;
    com.fxofficeapp.a_fit.Helper.CustomFont mainPhoneNumber;
    com.fxofficeapp.a_fit.Helper.CustomTextView depOneEmail;
    com.fxofficeapp.a_fit.Helper.CustomTextView depTwoEmail;
    ImageView depOnePhoneImg;
    ImageView depTwoPhoneImg;

    ScrollView contentFrame;


    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        contentFrame = (ScrollView)findViewById(R.id.content_wrapper);
        contentFrame.setVisibility(View.GONE);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.contacts);


        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        contactsTitle = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.contacts_title);
        contactsAddress = (com.fxofficeapp.a_fit.Helper.CustomTextView)findViewById(R.id.contact_address);
        depOneTitle = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.dep_one_title);
        depOnePhoneOne = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.dep_one_phone_one);
        depOnePhoneTwo = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.dep_one_phone_two);
        depTwoTitle = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.dep_two_title);
        depTwoPhoneOne = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.dep_two_phone_one);
        depTwoPhoneTwo = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.dep_two_phone_two);
        mainPhoneNumber = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.mainPhoneNumber);
        workDay = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.workday_lbl);
        weekEnd = (com.fxofficeapp.a_fit.Helper.CustomFont)findViewById(R.id.weekend_lbl);
        depOneEmail = (com.fxofficeapp.a_fit.Helper.CustomTextView)findViewById(R.id.dep_one_email);
        depTwoEmail = (com.fxofficeapp.a_fit.Helper.CustomTextView)findViewById(R.id.dep_two_email);
        depOnePhoneImg = (ImageView)findViewById(R.id.dep_one_phone_img);
        depTwoPhoneImg = (ImageView)findViewById(R.id.dep_two_phone_img);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        Double size = dm.heightPixels * 0.4;
        mapFragment.getView().getLayoutParams().height = size.intValue();


        parseData();
        checkTimeStamp();

    }

    void callaManager(final Contact contact) {

        LinearLayout callaManager = (LinearLayout) findViewById(R.id.callaManager);
        callaManager.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                String number = contact.getMainPhoneNumber().replaceAll("[^0-9+]","");
                callIntent.setData(Uri.parse("tel:" + contact.getMainPhoneNumber()));
                startActivity(callIntent);
            }
        });
    }

    void checkTimeStamp() {
        String contacts_data = preferences.getString("contacts_data_"+Constants.URL_SLUG, "");
        if(!contacts_data.equalsIgnoreCase(""))
        {
            try {

                JSONObject obj = new JSONObject(contacts_data);
                final int timeStampCached = obj.getInt("time");

                // Instantiate the RequestQueue.
                String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_CONTACTS;
                RequestQueue queue = Volley.newRequestQueue(this);


                Log.v("cache", "checking timestamp");

                // Request a string response from the provided URL.
                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                            try {

                                int timeStamp = response.getInt("time");

                                if (timeStampCached != timeStamp) {

                                    parseFreshData();
                                    Log.v("cache", "refresh data");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });

                // Add the request to the RequestQueue.
                queue.add(jsObjRequest);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    void parseData() {

        String contacts_data = preferences.getString("contacts_data_"+Constants.URL_SLUG, "");
        if(!contacts_data.equalsIgnoreCase(""))
        {

            Log.v("cache parse", "we are here");
            try {

                JSONObject obj = new JSONObject(contacts_data);

                updateModel(obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

            parseFreshData();

        }

    }

    void parseFreshData(){

        // Instantiate the RequestQueue.
        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_CONTACTS;
        RequestQueue queue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    editor.putString("contacts_data_"+Constants.URL_SLUG,response.toString());
                    editor.commit();

                    updateModel(response);

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
                    }
                });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);
    }

    void updateModel(JSONObject response){
        try {

            int timeStamp = response.getInt("time");
            String title = response.getString("title_club");
            String address = response.getString("adress");
            Double lng = response.getDouble("lng");
            Double lat = response.getDouble("lat");
            int zoom = response.getInt("zoom");

            JSONArray depOneArray = new JSONArray(response.getJSONArray("department").get(0).toString());
            JSONArray depTwoArray = new JSONArray(response.getJSONArray("department").get(1).toString());

            String depOneTitle = depOneArray.get(0).toString();
            String depOnePhoneOne = depOneArray.get(1).toString();
            String depOnePhoneTwo = depOneArray.get(2).toString();
            String depOneEmail = depOneArray.get(3).toString();
            String depTwoTitle = depTwoArray.get(0).toString();
            String depTwoPhoneOne = depTwoArray.get(1).toString();
            String depTwoPhoneTwo = depTwoArray.get(2).toString();
            String depTwoEmail = depTwoArray.get(3).toString();

            String mainPhoneNumber = response.getString("main_phone");

            JSONArray workDayArray = new JSONArray(response.getJSONArray("work_day").get(0).toString());
            JSONArray weekEndArray = new JSONArray(response.getJSONArray("work_day").get(1).toString());

            String workDay = workDayArray.get(0).toString();
            String weekEnd = weekEndArray.get(0).toString();

            contact = new Contact(timeStamp, title, address, lng, lat, zoom, depOneTitle,depOnePhoneOne,depOnePhoneTwo,depTwoTitle,depTwoPhoneOne,depTwoPhoneTwo,mainPhoneNumber,workDay,weekEnd,depOneEmail,depTwoEmail);

            updateUI(contact);

            contentFrame.setVisibility(View.VISIBLE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void updateUI(Contact contact){

        mapFragment.getMapAsync(this);

        if (!contact.getTitle().isEmpty()) {
            contactsTitle.setText(contact.getTitle().toUpperCase());
            contactsTitle.setVisibility(View.VISIBLE);
        } else {
            contactsTitle.setVisibility(View.GONE);
        }

        if (!contact.getAddress().isEmpty()) {
            contactsAddress.setText(contact.getAddress());
            contactsAddress.setVisibility(View.VISIBLE);
        } else {
            contactsAddress.setVisibility(View.GONE);
        }

        if (!contact.getDepOneTitle().isEmpty()) {
            depOneTitle.setText(contact.getDepOneTitle().toUpperCase());
            depOneTitle.setVisibility(View.VISIBLE);
        } else {
            depOneTitle.setVisibility(View.GONE);
        }

        if (!contact.getDepOnePhoneOne().isEmpty()) {
            depOnePhoneOne.setText(contact.getDepOnePhoneOne());
            depOnePhoneOne.setVisibility(View.VISIBLE);
            depOnePhoneImg.setVisibility(View.VISIBLE);
        } else {
            depOnePhoneOne.setVisibility(View.GONE);
            depOnePhoneImg.setVisibility(View.GONE);
        }

        if (!contact.getDepOnePhoneTwo().isEmpty()) {
            depOnePhoneTwo.setText(contact.getDepOnePhoneTwo());
            depOnePhoneTwo.setVisibility(View.VISIBLE);
        } else {
            depOnePhoneTwo.setVisibility(View.GONE);
        }

        if (!contact.getDepOneEmail().isEmpty()) {
            depOneEmail.setText(contact.getDepOneEmail());
        } else {
            depOneEmail.setVisibility(View.GONE);
        }

        if (!contact.getDepTwoTitle().isEmpty()) {
            depTwoTitle.setText(contact.getDepTwoTitle().toUpperCase());
            depTwoTitle.setVisibility(View.VISIBLE);
        } else {
            depTwoTitle.setVisibility(View.GONE);
        }

        if (!contact.getDepTwoPhoneOne().isEmpty()) {
            depTwoPhoneOne.setText(contact.getDepTwoPhoneOne());
            depTwoPhoneOne.setVisibility(View.VISIBLE);
            depTwoPhoneImg.setVisibility(View.VISIBLE);
        } else {
            depTwoPhoneOne.setVisibility(View.GONE);
            depTwoPhoneImg.setVisibility(View.GONE);
        }

        if (!contact.getDepTwoPhoneTwo().isEmpty()) {
            depTwoPhoneTwo.setText(contact.getDepTwoPhoneTwo());
            depTwoPhoneTwo.setVisibility(View.VISIBLE);
        } else {
            depTwoPhoneTwo.setVisibility(View.GONE);
        }

        if (!contact.getDepTwoEmail().isEmpty()) {
            depTwoEmail.setText(contact.getDepTwoEmail());
            depTwoEmail.setVisibility(View.VISIBLE);
        } else {
            depTwoEmail.setVisibility(View.GONE);
        }

        if (!contact.getMainPhoneNumber().isEmpty()) {
            mainPhoneNumber.setText(contact.getMainPhoneNumber());
            mainPhoneNumber.setVisibility(View.VISIBLE);
        } else {
            mainPhoneNumber.setVisibility(View.GONE);
        }

        if (!contact.getWorkDay().isEmpty()) {
            workDay.setText(contact.getWorkDay().toUpperCase());
            workDay.setVisibility(View.VISIBLE);
        } else {
            workDay.setVisibility(View.GONE);
        }

        if (!contact.getWeekEnd().isEmpty()) {
            weekEnd.setText(contact.getWeekEnd().toUpperCase());
            weekEnd.setVisibility(View.VISIBLE);
        } else {
            weekEnd.setVisibility(View.GONE);
        }



        callaManager(contact);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //56.3212, 43.9997
        double latitude  = contact.getLat();
        double longitude = contact.getLng();
        int zoom = contact.getZoom();

        // create marker
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(longitude, latitude)).title(getString(R.string.app_name).toUpperCase());

        // Changing marker icon
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_icon));

        mMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(longitude, latitude)).zoom(zoom).build();
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

    }

}

