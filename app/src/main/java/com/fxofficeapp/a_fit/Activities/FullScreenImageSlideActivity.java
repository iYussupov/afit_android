package com.fxofficeapp.a_fit.Activities;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

import com.fxofficeapp.a_fit.Helper.FullScreenImageAdapter;
import com.fxofficeapp.a_fit.R;

import java.util.ArrayList;

public class FullScreenImageSlideActivity extends Fragment {

    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;
    private int mLeftDelta;
    private int mTopDelta;
    private float mWidthScale;
    private float mHeightScale;

    private RelativeLayout frameLayout;
    private ColorDrawable colorDrawable;

    private int itemTop;
    private int itemLeft;
    private int itemWidth;
    private int itemHeight;
    private static final int ANIM_DURATION = 300;
    int position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_fullscreen_imageslide, container, false);

        View decorView = getActivity().getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        //Getting Image Uri
        position = getArguments().getInt("position", 0);
        ArrayList<String> mThumbIds = getArguments().getStringArrayList("imagesArray");

        Bundle bundle = getArguments();
        itemTop = bundle.getInt("top");
        itemLeft = bundle.getInt("left");
        itemWidth = bundle.getInt("width");
        itemHeight = bundle.getInt("height");

        if (mThumbIds != null) {

            //Set the background color to black
            frameLayout = (RelativeLayout) v.findViewById(R.id.main_background);
            colorDrawable = new ColorDrawable(Color.BLACK);
            frameLayout.setBackground(colorDrawable);

            adapter = new FullScreenImageAdapter(getActivity(), mThumbIds);

            viewPager = (ViewPager) v.findViewById(R.id.pager);
            viewPager.setAdapter(adapter);

            // displaying selected image first
            viewPager.setCurrentItem(position);

            if (savedInstanceState == null) {
                ViewTreeObserver observer = viewPager.getViewTreeObserver();
                observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        viewPager.getViewTreeObserver().removeOnPreDrawListener(this);

                        // Figure out where the thumbnail and full size versions are, relative
                        // to the screen and each other
                        int[] screenLocation = new int[2];
                        viewPager.getLocationOnScreen(screenLocation);
                        mLeftDelta = itemLeft - screenLocation[0];
                        mTopDelta = itemTop - screenLocation[1];

                        // Scale factors to make the large version the same size as the thumbnail
                        mWidthScale = (float) itemWidth / viewPager.getWidth();
                        mHeightScale = (float) itemHeight / viewPager.getHeight();

                        enterAnimation();

                        return true;
                    }
                });
            }

        }
        return v;
    }

    public void enterAnimation() {

        // Set starting values for properties we're going to animate. These
        // values scale and position the full size version down to the thumbnail
        // size/location, from which we'll animate it back up
        viewPager.setPivotX(0);
        viewPager.setPivotY(0);
        viewPager.setScaleX(mWidthScale);
        viewPager.setScaleY(mHeightScale);
        viewPager.setTranslationX(mLeftDelta);
        viewPager.setTranslationY(mTopDelta);

        // interpolator where the rate of change starts out quickly and then decelerates.
        TimeInterpolator sDecelerator = new DecelerateInterpolator();

        // Animate scale and translation to go from thumbnail to full size
        viewPager.animate().setDuration(ANIM_DURATION).scaleX(1).scaleY(1).
                translationX(0).translationY(0).setInterpolator(sDecelerator);

        // Fade in the black background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0, 255);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();

    }

    public int getCurrentImgPosition() {
        return viewPager.getCurrentItem();
    }

    public void exitAnimation(final Runnable endAction, int lPos, int tPos) {
        TimeInterpolator sInterpolator = new AccelerateInterpolator();

        int[] screenLocation = new int[2];
        viewPager.getLocationOnScreen(screenLocation);
        mLeftDelta = lPos - screenLocation[0];
        mTopDelta = tPos - screenLocation[1];

        viewPager.animate().setDuration(ANIM_DURATION).scaleX(mWidthScale).scaleY(mHeightScale).
                translationX(mLeftDelta).translationY(mTopDelta)
                .setInterpolator(sInterpolator).withEndAction(endAction);

        // Fade out background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();
    }

}
