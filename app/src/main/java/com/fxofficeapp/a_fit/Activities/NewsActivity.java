package com.fxofficeapp.a_fit.Activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.CustomClickListener;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.fxofficeapp.a_fit.Helper.EndlessRecyclerViewScrollListener;
import com.fxofficeapp.a_fit.Helper.NewsListAdapter;
import com.fxofficeapp.a_fit.Models.Post;
import com.fxofficeapp.a_fit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewsActivity extends DrawerActivity {

    private List<Post> postList = new ArrayList<>();
    private RecyclerView recyclerView;
    private NewsListAdapter mAdapter;
    private Post post_data;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private LinearLayoutManager mLayoutManager;
    private EndlessRecyclerViewScrollListener scrollListener;
    private int postsPerPage = 10;
    private int page = 1;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean shoudBeRefreshed = false;
    private boolean updatingNow = false;
    public boolean viewInitialized = false;
    LinearLayout contentFrame;
    float contentFrameWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.news_offers);

        contentFrame = (LinearLayout)findViewById(R.id.content_frame);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.newsRecyclerView);

        mAdapter = new NewsListAdapter(postList);
        mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(false);

        scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                parseFreshData(page);
            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);

        recyclerView.addOnItemTouchListener(new CustomClickListener.RecyclerTouchListener(getApplicationContext(), recyclerView, new CustomClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Post post = postList.get(position);

                ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                        PropertyValuesHolder.ofFloat("translationX", 0f, -300.0f)
                );

                translateX.setDuration(200);
                translateX.setStartDelay(100);
                translateX.setInterpolator(new AccelerateDecelerateInterpolator());
                translateX.start();

                android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putSerializable("post", post);
                NewsDetailActivity f = new NewsDetailActivity();
                f.setArguments(bundle);
                ft.setCustomAnimations(R.animator.slide_left, R.animator.slide_right,R.animator.slide_left, R.animator.slide_right);
                ft.add(R.id.fragment_wrapper, f);
                ft.addToBackStack("post");
                ft.commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        parseFreshData(page);

    }

    public void onResume()
    {
        super.onResume();

    }

    @Override
    public void onBackPressed() {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View decorView = this.getWindow().getDecorView();
        // Show the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
        decorView.setSystemUiVisibility(uiOptions);
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();

            ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                    PropertyValuesHolder.ofFloat("translationX", -300.0f, 0f)
            );

            translateX.setDuration(200);
            translateX.setInterpolator(new AccelerateDecelerateInterpolator());
            translateX.start();
        }

    }

    void parseOldData(int page) {

        mSwipeRefreshLayout.setRefreshing(false);

        String posts_data = preferences.getString("posts_data"+page+"_"+Constants.URL_SLUG, "");

        if(!posts_data.equalsIgnoreCase(""))
        {

            if (page == 1) {
                Toast.makeText(getApplicationContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
            }
            Log.v("cache parse", "we are here");
                try {

                    JSONArray obj = new JSONArray(posts_data);

                    updateModel(obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
        } else {
            Toast.makeText(getApplicationContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
        }

    }

    void parseFreshData(final int page){

        if (page == 1) {
            mSwipeRefreshLayout.setRefreshing(true);
        }

        final RequestQueue queue = Volley.newRequestQueue(this);
        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_POSTS + "?per_page="+ postsPerPage +"&page="+ page;

        JsonArrayRequest jsonRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                    editor.putString("posts_data"+page+"_"+Constants.URL_SLUG,response.toString());
                    editor.commit();

                    Log.v("PAGE",String.valueOf(page));
                    updateModel(response);
                    mAdapter.notifyDataSetChanged();
                        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                viewInitialized = true;
                            }
                        });

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                        }, 500);

                    Log.v("DATA","UPDATED");

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        parseOldData(page);
                    }
                });
        // Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }

    void updateModel(JSONArray response){
        for (int i = 0; i < response.length(); i++) {
            try {
                final JSONObject jsonData = response.getJSONObject(i);
                
                String category = jsonData.getString("post_category_name");

                String postTitle = "";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    postTitle = Html.fromHtml(jsonData.getJSONObject("title").getString("rendered"),Html.FROM_HTML_MODE_LEGACY).toString();
                } else {
                    postTitle = Html.fromHtml(jsonData.getJSONObject("title").getString("rendered")).toString();
                }

                String content = "";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    content = Html.fromHtml(jsonData.getJSONObject("content").getString("rendered"),Html.FROM_HTML_MODE_LEGACY).toString();
                } else {
                    content = Html.fromHtml(jsonData.getJSONObject("content").getString("rendered")).toString();
                }
                String img = jsonData.getString("featured_image_thumbnail_url");
                String date = "";

                String dtStart = jsonData.getString("date");
                try {

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date newDate = format.parse(dtStart);
                    format = new SimpleDateFormat("dd.MM.yyyy");
                    date = format.format(newDate);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                post_data = new Post(postTitle, date, category, img, content);
                postList.add(post_data);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void refreshItems() {
        // Load items
        Log.v("refreshItems","TRIGGERED");
        postList.removeAll(postList);
        scrollListener.resetState();
        shoudBeRefreshed = true;
        parseFreshData(page);

        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed


        // Stop refresh animation
        mSwipeRefreshLayout.setRefreshing(false);
    }

}
