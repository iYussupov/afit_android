package com.fxofficeapp.a_fit.Activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.CustomClickListener;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.fxofficeapp.a_fit.Helper.DividerItemDecoration;
import com.fxofficeapp.a_fit.Helper.ServicesListAdapter;
import com.fxofficeapp.a_fit.Models.Services;
import com.fxofficeapp.a_fit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ServicesActivity extends DrawerActivity {


    private List<Services> servicesList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ServicesListAdapter mAdapter;
    private Services service_data;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public boolean viewInitialized = false;
    LinearLayout contentFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.services);

        contentFrame = (LinearLayout)findViewById(R.id.content_frame);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        recyclerView = (RecyclerView)findViewById(R.id.servicesRecyclerView);

        mAdapter = new ServicesListAdapter(servicesList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(false);

        recyclerView.addOnItemTouchListener(new CustomClickListener.RecyclerTouchListener(getApplicationContext(), recyclerView, new CustomClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Services service = servicesList.get(position);
                ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                        PropertyValuesHolder.ofFloat("translationX", 0f, -300.0f)
                );

                translateX.setDuration(200);
                translateX.setStartDelay(100);
                translateX.setInterpolator(new AccelerateDecelerateInterpolator());
                translateX.start();
                android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putSerializable("service", service);
                ServiceDetailActivity f = new ServiceDetailActivity();
                f.setArguments(bundle);
                ft.setCustomAnimations(R.animator.slide_left, R.animator.slide_right,R.animator.slide_left, R.animator.slide_right);
                ft.add(R.id.fragment_wrapper, f);
                ft.addToBackStack("service");
                ft.commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        parseData();
        checkTimeStamp();

    }

    void checkTimeStamp() {
        String services_data = preferences.getString("services_data_"+Constants.URL_SLUG, "");
        if(!services_data.equalsIgnoreCase(""))
        {
            try {

                JSONObject obj = new JSONObject(services_data);
                final int timeStampCached = obj.getInt("time");

                // Instantiate the RequestQueue.
                String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_SERVICES;
                RequestQueue queue = Volley.newRequestQueue(this);

                Log.v("cache", "checking timestamp");

                // Request a string response from the provided URL.
                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                try {

                                    int timeStamp = response.getInt("time");

                                    if (timeStampCached != timeStamp) {

                                        parseFreshData();
                                        Log.v("cache", "refresh data");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });

                // Add the request to the RequestQueue.
                queue.add(jsObjRequest);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void parseData() {

        String services_data = preferences.getString("services_data_"+Constants.URL_SLUG, "");
        if(!services_data.equalsIgnoreCase(""))
        {

            Log.v("cache parse", "we are here");
            try {

                JSONObject obj = new JSONObject(services_data);
                updateModel(obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

            parseFreshData();

        }

    }

    void parseFreshData(){

        // Instantiate the RequestQueue.
        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_SERVICES;
        RequestQueue queue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        editor.putString("services_data_"+Constants.URL_SLUG,response.toString());
                        editor.commit();

                        updateModel(response);

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
                    }
                });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);
    }

    void updateModel(JSONObject response){

        try {

            int timeStamp = response.getInt("time");
            JSONArray services = response.getJSONArray("services");
            servicesList.clear();

            for (int i = 0; i < services.length(); i++) {
                final JSONObject jsonData = services.getJSONObject(i);

                String title = jsonData.getString("service_name");
                String content = jsonData.getString("service_description");
                JSONArray imgArray = jsonData.getJSONArray("service_images");
                String thumb = "";
                for (int index = 0; index<imgArray.length(); index++) {
                    JSONObject img = imgArray.getJSONObject(index);
                    thumb = img.getString("image_url");
                }

                String icon = "";

                service_data = new Services(timeStamp, title, content, icon, thumb);
                servicesList.add(service_data);

                mAdapter.notifyDataSetChanged();

                recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        viewInitialized = true;
                    }
                });

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View decorView = this.getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
        decorView.setSystemUiVisibility(uiOptions);
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
            ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                    PropertyValuesHolder.ofFloat("translationX", -300.0f, 0f)
            );

            translateX.setDuration(200);
            translateX.setInterpolator(new AccelerateDecelerateInterpolator());
            translateX.start();
        }

    }
}
