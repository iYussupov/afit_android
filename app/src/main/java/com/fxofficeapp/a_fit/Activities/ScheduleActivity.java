package com.fxofficeapp.a_fit.Activities;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Calendar.fragment.WeekFragment;
import com.fxofficeapp.a_fit.Calendar.listener.OnWeekChangeListener;
import com.fxofficeapp.a_fit.Helper.App;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.CustomFont;

import com.fxofficeapp.a_fit.Calendar.WeekCalendar;
import com.fxofficeapp.a_fit.Calendar.listener.OnDateClickListener;
import com.fxofficeapp.a_fit.Helper.ScheduleListAdapter;
import com.fxofficeapp.a_fit.Models.Schedule;
import com.fxofficeapp.a_fit.R;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ScheduleActivity extends DrawerActivity {

    private List<Schedule> scheduleList = new ArrayList<>();
    private static List<Schedule> scheduleListToShow = new ArrayList<>();
    private static List<Schedule> scheduleTemp = new ArrayList<>();
    private static RecyclerView recyclerView;
    private static ScheduleListAdapter mAdapter;
    private static WeekCalendar weekCalendar;
    private Schedule schedule;
    private static DateTime dateClicked;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    static int selectedDayOfWeek;

    Animation inForward = null;
    static Animation outForward = null;

    Animation inBack = null;
    static Animation outBack = null;

    static boolean daySwiped = false;
    private boolean scheduleInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        JodaTimeAndroid.init(this);


        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.schedule);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        weekCalendar = (WeekCalendar)findViewById(R.id.weekCalendar);

        final int currentDayOfWeek = weekCalendar.getCurrentDayOfWeek();

        selectedDayOfWeek = currentDayOfWeek;

        weekCalendar.reset();

        inForward = AnimationUtils.makeInAnimation(getApplicationContext(),false);
        inForward.setDuration(250);
        outForward = AnimationUtils.makeOutAnimation(getApplicationContext(),false);
        outForward.setDuration(250);
        inBack = AnimationUtils.makeInAnimation(getApplicationContext(),true);
        inBack.setDuration(250);
        outBack = AnimationUtils.makeOutAnimation(getApplicationContext(),true);
        outBack.setDuration(250);

        outForward.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                recyclerView.setAdapter(null);

                addScheduleToWeekday(selectedDayOfWeek, scheduleList);

                recyclerView.startAnimation(inForward);
                daySwiped = false;
            }
        });

        outBack.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                recyclerView.setAdapter(null);

                addScheduleToWeekday(selectedDayOfWeek, scheduleList);

                recyclerView.startAnimation(inBack);
                daySwiped = false;
            }
        });

        scheduleTemp.removeAll(scheduleTemp);

        recyclerView = (RecyclerView)findViewById(R.id.scheduleRecyclerView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setHasFixedSize(false);

        // set the adapter
        recyclerView.setAdapter(mAdapter);
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                scheduleInitialized = true;
            }
        });

        weekCalendar.setOnWeekChangeListener(new OnWeekChangeListener() {
            @Override
            public void onWeekChange(final DateTime firstDayOfTheWeek, final boolean forward) {

                if (!daySwiped) {
                    WeekFragment.selectedDateTime = firstDayOfTheWeek;
                }
                weekCalendar.updateUi();
                dateClicked = firstDayOfTheWeek;

                selectedDayOfWeek = firstDayOfTheWeek.getDayOfWeek();

                if (forward) {
                    recyclerView.startAnimation(outForward);
                } else {
                    recyclerView.startAnimation(outBack);
                }
            }
        });

        weekCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(DateTime dateTime) {
                boolean forward = false;
                if (selectedDayOfWeek < dateTime.getDayOfWeek()) {
                    forward = true;
                }
                selectedDayOfWeek = dateTime.getDayOfWeek();

                dateClicked = dateTime;

                if (forward) {
                    recyclerView.startAnimation(outForward);
                } else {
                    recyclerView.startAnimation(outBack);
                }
            }

        });

        parseData(currentDayOfWeek);
        parseClassesData();
        checkTimeStamp(currentDayOfWeek);

    }

    public static void SwipeScheduleLeft(){
        daySwiped = true;
        weekCalendar.moveToNext();
        dateClicked = WeekFragment.selectedDateTime;
        selectedDayOfWeek = WeekFragment.selectedDateTime.getDayOfWeek();
        recyclerView.startAnimation(outForward);
    }

    public static void SwipeScheduleRight(){
        daySwiped = true;
        weekCalendar.moveToPrevious();
        dateClicked = WeekFragment.selectedDateTime;
        selectedDayOfWeek = WeekFragment.selectedDateTime.getDayOfWeek();
        recyclerView.startAnimation(outBack);
    }

    void checkTimeStamp(final int currentDayOfWeek) {
        String schedule_data = preferences.getString("schedule_data_"+Constants.URL_SLUG, "");
        if(!schedule_data.equalsIgnoreCase(""))
        {
            try {

                JSONObject obj = new JSONObject(schedule_data);
                final int timeStampCached = obj.getInt("time");

                // Instantiate the RequestQueue.
                String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_SCHEDULE;
                RequestQueue queue = Volley.newRequestQueue(this);


                Log.v("cache", "checking timestamp");

                // Request a string response from the provided URL.
                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                try {

                                    int timeStamp = response.getInt("time");
                                    Log.v("cache", String.valueOf(timeStampCached));
                                    Log.v("cache", String.valueOf(timeStamp));
                                    if (timeStampCached != timeStamp) {
                                        scheduleList.removeAll(scheduleList);
                                        parseFreshData(currentDayOfWeek);
                                        mAdapter.notifyDataSetChanged();
                                        Log.v("cache", "refresh data");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });

                // Add the request to the RequestQueue.
                queue.add(jsObjRequest);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void parseData(final int currentDayOfWeek) {

        String schedules_data = preferences.getString("schedule_data_"+Constants.URL_SLUG, "");
        if(!schedules_data.equalsIgnoreCase(""))
        {
            try {
                JSONObject obj = new JSONObject(schedules_data);
                Log.v("cache parse", "we are here");

                updateModel(obj, currentDayOfWeek);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            parseFreshData(currentDayOfWeek);

        }

    }

    private void parseFreshData(final int currentDayOfWeek) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_SCHEDULE;

        // Request a string response from the provided URL.
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        editor.putString("schedule_data_"+Constants.URL_SLUG,response.toString());
                        editor.commit();

                        updateModel(response, currentDayOfWeek);

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
                    }
                });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);
    }

    void updateModel(JSONObject response, final int currentDayOfWeek){

        try {

            JSONObject scheduleObj = response.getJSONObject("schedule");
            final int timeStamp = response.getInt("time");

            Iterator<String> iter = scheduleObj.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    JSONArray object = (JSONArray) scheduleObj.get(key);
                    for(int i=0;i<object.length(); i++){

                        JSONObject json_data = object.getJSONObject(i);

                        int weekday       = Integer.valueOf(json_data.getString("weekday"));

                        String title = "";
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            title = Html.fromHtml(json_data.getString("class_title"),Html.FROM_HTML_MODE_LEGACY).toString();
                        } else {
                            title = Html.fromHtml(json_data.getString("class_title")).toString();
                        }

                        String content = "";
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            content = Html.fromHtml(json_data.getString("class_desc"),Html.FROM_HTML_MODE_LEGACY).toString();
                        } else {
                            content = Html.fromHtml(json_data.getString("class_desc")).toString();
                        }

                        String place = "";
                        if (!json_data.getString("location_title").equals("-")){
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                place = Html.fromHtml(json_data.getString("location_title"),Html.FROM_HTML_MODE_LEGACY).toString();
                            } else {
                                place = Html.fromHtml(json_data.getString("location_title")).toString();
                            }
                        }
                        String start_time = json_data.getString("start_time");
                        String start_date = json_data.getString("start_date");
                        String end_date   = json_data.getString("end_date");
                        String duration   = json_data.getString("duration");
                        schedule = new Schedule(timeStamp, weekday, title, content, place, start_time, start_date, end_date, duration);
                        scheduleList.add(schedule);

                    }

                    addScheduleToWeekday(currentDayOfWeek, scheduleList);


                } catch (JSONException e) {
                    // Something went wrong!
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addScheduleToWeekday(final int currentDayOfWeek, List<Schedule> scheduleList) {

        scheduleTemp.removeAll(scheduleTemp);
        scheduleListToShow.removeAll(scheduleListToShow);

        int weekd = currentDayOfWeek;

        if (weekd == 7) weekd = 0;

        DateTime today = DateTime.now();

        DateTime now = new DateTime();
        DateTime yesterday = now.minusDays(1);

        if (dateClicked == null) dateClicked = today;

        for (int i=0; i < scheduleList.size(); i++){

            Schedule singleSchedule = scheduleList.get(i);
            int weekday = singleSchedule.getWeekday();

            String start_date_string = singleSchedule.getStartDate();
            String end_date_string = singleSchedule.getEndDate();
            DateTimeFormatter format = DateTimeFormat.forPattern("dd-MM-yyyy");
            DateTime start_date = null;
            DateTime end_date = null;

            if (!start_date_string.isEmpty()) {
                start_date = DateTime.parse(start_date_string, format);
            }
            if (!end_date_string.isEmpty()) {
                end_date = DateTime.parse(end_date_string, format);
            }

            if (start_date != null && dateClicked.compareTo(start_date) > 0) {

                if (end_date != null) {

                    if (dateClicked.compareTo(end_date.plusDays(1)) < 0) {

                        if (weekday == weekd) {
                            scheduleListToShow.add(singleSchedule);
                        }

                    }

                } else {

                    if (weekday == weekd) {
                        scheduleListToShow.add(singleSchedule);
                    }

                }
            }



        }


        scheduleTemp.addAll(scheduleListToShow);
        mAdapter = new ScheduleListAdapter(scheduleListToShow, scheduleInitialized);
        recyclerView.setAdapter(mAdapter);
        scheduleDidFiltered();
    }

    public void scheduleDidFiltered() {

        scheduleListToShow.removeAll(scheduleListToShow);

        if (Constants.TRAINING_FILTER_NAME != Constants.DEFAULT_FILTER_NAME) {

            for (int i=0; i< scheduleTemp.size(); i++) {
                String scheduleTitle = scheduleTemp.get(i).getTitle();
                if (scheduleTitle.equals(Constants.TRAINING_FILTER_NAME)) {
                    scheduleListToShow.add(scheduleTemp.get(i));
                }
            }

        } else {

            scheduleListToShow.addAll(scheduleTemp);

        }
        mAdapter.notifyDataSetChanged();
    }

    public void showSignUpModal(View anchorView, final Schedule schedule) {


        final View popupView = getLayoutInflater().inflate(R.layout.fragment_training_sign_up, null);

        final PopupWindow popupWindow = new PopupWindow(popupView,
                RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT);

        com.fxofficeapp.a_fit.Helper.CustomFont cancelBtn = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.cancelBtn);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        com.fxofficeapp.a_fit.Helper.CustomFont signUpDate = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.signup_date);
        com.fxofficeapp.a_fit.Helper.CustomFont trainingTime = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.training_time);
        com.fxofficeapp.a_fit.Helper.CustomFont trainingName = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.training_name);
        com.fxofficeapp.a_fit.Helper.CustomFont trainingPlace = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.training_place);
        com.fxofficeapp.a_fit.Helper.CustomFont trainingDuration = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.training_duration);


        DateTime dt = dateClicked;
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.yyyy");
        final String currentDate = fmt.print(dt);

        signUpDate.setText(currentDate);
        trainingTime.setText(schedule.getStartTime());
        trainingName.setText(schedule.getTitle().toUpperCase());

        if (schedule.getPlace() != ""){
            trainingPlace.setText(schedule.getPlace().toUpperCase());
            trainingPlace.setVisibility(View.VISIBLE);
        } else {
            trainingPlace.setVisibility(View.GONE);
        }

        trainingDuration.setText(schedule.getDuration()+ " " + getResources().getString(R.string.min).toUpperCase());

        LinearLayout sendMailBtn = (LinearLayout) popupView.findViewById(R.id.sendSignupBtn);

        sendMailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.fxofficeapp.a_fit.Helper.CustomEditText client_name = (com.fxofficeapp.a_fit.Helper.CustomEditText) popupView.findViewById(R.id.client_name);
                com.fxofficeapp.a_fit.Helper.CustomEditText client_phone = (com.fxofficeapp.a_fit.Helper.CustomEditText) popupView.findViewById(R.id.client_phone);

                String clientName = client_name.getText().toString();
                String clientPhone = client_phone.getText().toString();
                if (!clientName.matches("") & !clientPhone.matches("") ) {
                    sendMail(popupWindow, clientName, clientPhone, schedule, currentDate);
                } else {
                    Toast.makeText(App.getContext(), getResources().getString(R.string.feel_fills), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });


        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);

        // If you need the PopupWindow to dismiss when when touched outside
//        popupWindow.setBackgroundDrawable(new ColorDrawable());


        DateTime today = DateTime.now();

        DateTime now = new DateTime();
        DateTime yesterday = now.minusDays(1);

        if (dateClicked == null) dateClicked = today;

        Log.v("dateClicked", dateClicked.toString());

        if (dateClicked.compareTo(yesterday) > 0) {
            popupWindow.setAnimationStyle(R.style.popupanim);
            popupWindow.showAtLocation((this).getWindow().getDecorView(), Gravity.NO_GRAVITY, 0, 0);

        } else {

            Toast.makeText(App.getContext(), getResources().getString(R.string.toolate), Toast.LENGTH_SHORT).show();

        }


    }

    private void sendMail(final PopupWindow popupWindow, String clientName, String clientPhone, final Schedule schedule, String currentDate) {
        Log.i("Sending email","Now");

        RequestQueue queue = Volley.newRequestQueue(App.getContext());
        String client_name = Uri.encode(clientName);
        String client_phone = Uri.encode(clientPhone);
        String training_name = Uri.encode(schedule.getTitle());
        String training_time = Uri.encode(schedule.getStartTime());
        String training_date = Uri.encode(currentDate);
        String token = Constants.DEVICE_TOKEN;

        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_MAILER + "?action=schedule&device=android&token="+token+"&client_phone="+ client_phone +"&client_name="+ client_name +"&training_name="+ training_name +"&training_time="+ training_time +"&training_date="+ training_date;

        // Request a string response from the provided URL.
        StringRequest mailSendRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(App.getContext(), getResources().getString(R.string.confirmed), Toast.LENGTH_SHORT).show();
                popupWindow.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
            }
        });
        // Add the request to the RequestQueue.
        queue.add(mailSendRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.schedule_menu, menu);
        return true;
    }

    public void parseClassesData() {

        final String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_CLASSES;
        RequestQueue queue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        JsonObjectRequest jsonObj = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            int timeStamp = response.getInt("time");

                            editor.putInt("classes_time_created_"+Constants.URL_SLUG, timeStamp);
                            editor.putString("classes_data_"+Constants.URL_SLUG, response.toString());
                            editor.commit();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        // Add the request to the RequestQueue.
        queue.add(jsonObj);
    }
}
