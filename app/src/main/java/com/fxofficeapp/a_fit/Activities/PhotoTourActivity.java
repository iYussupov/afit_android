package com.fxofficeapp.a_fit.Activities;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.PhotoTourGridAdapter;
import com.fxofficeapp.a_fit.Models.Photo;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class PhotoTourActivity extends DrawerActivity {

    private PhotoTourGridAdapter adapter;
    private GridView gridView;
    private Photo photo;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ArrayList<String> IMAGES = new ArrayList<String>();
    public boolean viewInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_tour);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.photo_tour);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        gridView = (GridView) findViewById(R.id.grid_view);

        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int size = dm.widthPixels / 3 - 32;

        // Gridview adapter
        adapter = new PhotoTourGridAdapter(this, IMAGES, size);

        // setting grid view adapter
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(gridviewOnItemClickListener);

        parseData();
        checkTimeStamp();
    }

    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {

            int[] screenLocation = new int[2];
            v.getLocationOnScreen(screenLocation);

            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            bundle.putStringArrayList("imagesArray", IMAGES);

            bundle.putInt("left", screenLocation[0]);
            bundle.putInt("top", screenLocation[1]);
            bundle.putInt("width", v.getWidth());
            bundle.putInt("height", v.getHeight());

            FullScreenImageSlideActivity f = new FullScreenImageSlideActivity();
            f.setArguments(bundle);
//            ft.setCustomAnimations(R.animator.alpha_in, R.animator.alpha_out,R.animator.alpha_in, R.animator.alpha_out);
            ft.add(android.R.id.content, f);
            ft.addToBackStack("phototour");
            ft.commit();
        }
    };

    void checkTimeStamp() {
        String phototour_data = preferences.getString("phototour_data_"+Constants.URL_SLUG, "");
        if(!phototour_data.equalsIgnoreCase(""))
        {
            try {

                JSONObject obj = new JSONObject(phototour_data);
                final int timeStampCached = obj.getInt("time");

                // Instantiate the RequestQueue.
                String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_PHOTO;
                RequestQueue queue = Volley.newRequestQueue(this);

                // Request a string response from the provided URL.
                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                try {

                                    int timeStamp = response.getInt("time");

                                    if (timeStampCached != timeStamp) {

                                        parseFreshData();

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        });

                // Add the request to the RequestQueue.
                queue.add(jsObjRequest);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void parseData() {

        String phototour_data = preferences.getString("phototour_data_"+Constants.URL_SLUG, "");
        if(!phototour_data.equalsIgnoreCase(""))
        {
            try {

                JSONObject obj = new JSONObject(phototour_data);
                int timeStamp = obj.getInt("time");
                JSONObject images = obj.getJSONObject("phototour");

                photo = new Photo(timeStamp, images);

                updateUI(photo);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

            parseFreshData();

        }

    }

    void parseFreshData(){

        // Instantiate the RequestQueue.
        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_PHOTO;
        RequestQueue queue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        editor.putString("phototour_data_"+Constants.URL_SLUG,response.toString());
                        editor.commit();

                        try {

                            int timeStamp = response.getInt("time");
                            JSONObject images = response.getJSONObject("phototour");

                            photo = new Photo(timeStamp, images);

                            updateUI(photo);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
                    }
                });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);

    };

    void updateUI(Photo photo){

        IMAGES.clear();
        JSONObject images = photo.getImageUrl();

        Iterator<String> iter = images.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                String img = (String) images.get(key);
                IMAGES.add(img);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter.notifyDataSetChanged();
        gridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                viewInitialized = true;
            }
        });

    }

    @Override
    public void onBackPressed() {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View decorView = this.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
        decorView.setSystemUiVisibility(uiOptions);

        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {

            FullScreenImageSlideActivity fragment = (FullScreenImageSlideActivity) getFragmentManager().findFragmentById(android.R.id.content);


            int item = fragment.getCurrentImgPosition();
            View itemView = gridView.getChildAt(item);
            int[] screenLocation = new int[2];
            itemView.getLocationOnScreen(screenLocation);

            int lPos = screenLocation[0];
            int tPos = screenLocation[1];

            fragment.exitAnimation(new Runnable() {
                public void run() {
                    getFragmentManager().popBackStack();
                }
            }, lPos, tPos);
        }

    }
}
