package com.fxofficeapp.a_fit.Activities;


import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fxofficeapp.a_fit.Fragments.LaunchFragment;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.ShowPopUp;
import com.fxofficeapp.a_fit.Helper.App;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.fxofficeapp.a_fit.Helper.FirebaseIDService;
import com.fxofficeapp.a_fit.Helper.MainMenuAdapter;
import com.fxofficeapp.a_fit.Models.Menu;
import com.fxofficeapp.a_fit.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends DrawerActivity {

    public static boolean startedFlag = false;

    private List<Menu> menuList = new ArrayList<>();
    private MainMenuAdapter gAdapter;
    View c;
    LinearLayout clubSelectLbl;
    Window window;
    int alpha = 0;
    int temp = 0;
    private static CustomFont appTitle, clubFamilyTitle;
    Activity activity = this;
    GridView gridview;
    Toolbar mToolbar;
    public boolean viewInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setTextColor(Color.argb(0,255,255,255));
        appTitle.setText(R.string.app_name);

        window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        gridview = (GridView) findViewById(R.id.gridview);
        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int size = dm.widthPixels / 2 - 42;
        gAdapter = new MainMenuAdapter(this, menuList, size);
        gridview.setAdapter(gAdapter);

        clubSelectLbl = (LinearLayout)findViewById(R.id.clubSelectLbl);
        clubFamilyTitle = (CustomFont)findViewById(R.id.clubFamilyTitle);

        if(!startedFlag) {
            //Launch screen - should work at once
            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(android.R.id.content, new LaunchFragment());
            ft.commit();

            gridview.setVisibility(View.GONE);
            startedFlag = true;

            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.d("notif", "get device token: " + refreshedToken);

            FirebaseMessaging.getInstance().subscribeToTopic("test");
            FirebaseIDService.registerToken(refreshedToken);

        }

        if (Constants.APP_FAMILY_ARRAY.length() < 2) {
            clubSelectLbl.setVisibility(View.GONE);

            final float scale = getResources().getDisplayMetrics().density;
            int padding = (int) (16 * scale + 0.5f);
            int padding_top = (int) (0 * scale + 0.5f);
            gridview.setPadding(padding,padding_top,padding,padding);

        } else {

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            final SharedPreferences.Editor editor = preferences.edit();

            boolean first_start = preferences.getBoolean("first_start", true);
            if(first_start)
            {

                final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                        .findViewById(android.R.id.content)).getChildAt(0);
                viewGroup.post(new Runnable() {
                    public void run() {
                        new ShowPopUp(App.getContext()).clubSelect(viewGroup, activity);
                        editor.putBoolean("first_start",false);
                        editor.commit();
                    }
                });
            }

        }

        clubFamilyTitle.setText(Constants.CURRENT_CLUB_NAME.toUpperCase());

        clubFamilyTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ShowPopUp(App.getContext()).clubSelect(v, activity);
            }
        });

        if (!(menuList.size() > 0)) {
            preparePostData();
        }

        gridview.setOnItemClickListener(gridviewOnItemClickListener);

        gridview.setOnScrollListener( new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                c = gridview.getChildAt(0);
                if (c != null) {
                    int offset = -c.getTop() + gridview.getFirstVisiblePosition() * c.getHeight();
                    clubSelectLbl.setTranslationY(-offset);

                    alpha = offset;
                    if (alpha>230){alpha = 255;} else if (alpha<30){alpha = 0;};

                    int color = getResources().getColor(R.color.colorPrimary);
                    int red = Color.red(color);
                    int green = Color.green(color);
                    int blue = Color.blue(color);

                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.argb(alpha,red,green,blue)));

                    appTitle.setTextColor(Color.argb(alpha,255,255,255));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        if (alpha > 0) {
                            window.setStatusBarColor(App.getContext().getResources().getColor(R.color.colorPrimaryDark));
                        } else {
                            window.setStatusBarColor(App.getContext().getResources().getColor(R.color.transparent));
                        }
                    }

                }

            }
        });


    }

    public void updatePanelsUI(){
        gridview.setVisibility(View.VISIBLE);
        ((BaseAdapter) gridview.getAdapter()).notifyDataSetChanged();
        gridview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                viewInitialized = true;
            }
        });
    }

    public void updateClubSelectTitle() {
        clubFamilyTitle.setText(Constants.CURRENT_CLUB_NAME.toUpperCase());
    }

    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View v, final int position,
                                long id) {

            for (int i=0; i< menuList.size(); i++ ){

                ViewGroup vg = (ViewGroup) parent.getChildAt(i);

                ObjectAnimator vgFadeOut = ObjectAnimator.ofPropertyValuesHolder(vg,
                        PropertyValuesHolder.ofFloat("alpha", 1.0f, 0f));

                vgFadeOut.setDuration(100);
                vgFadeOut.start();

            }

            float scaleFactor = 2f;
            float sizeVariation = scaleFactor*120;

            ObjectAnimator gridItemFromRight = ObjectAnimator.ofPropertyValuesHolder(v,
                PropertyValuesHolder.ofFloat("scaleX", scaleFactor),
                PropertyValuesHolder.ofFloat("scaleY", scaleFactor),
                PropertyValuesHolder.ofFloat("translationY", 0f, sizeVariation),
                PropertyValuesHolder.ofFloat("translationX", 0f, -sizeVariation)
            );
            ObjectAnimator gridItemFromLeft = ObjectAnimator.ofPropertyValuesHolder(v,
                PropertyValuesHolder.ofFloat("scaleX", scaleFactor),
                PropertyValuesHolder.ofFloat("scaleY", scaleFactor),
                PropertyValuesHolder.ofFloat("translationY", 0f, sizeVariation),
                PropertyValuesHolder.ofFloat("translationX", 0f, sizeVariation)
            );

            gridItemFromRight.setDuration(100);
            gridItemFromLeft.setDuration(100);

            if ((position & 0x01) != 0) {
                gridItemFromRight.start();
            } else {
                gridItemFromLeft.start();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadActivity(position);
                }
            }, 250);
        }
    };

    private void  loadActivity(int position){

        Class activity = MainActivity.class;

        switch(position) {
            case 0:
                activity = ScheduleActivity.class;
                break;
            case 1:
                activity = NewsActivity.class;
                break;
            case 2:
                activity = ServicesActivity.class;
                break;
            case 3:
                activity = PushActivity.class;
                break;
            case 4:
                activity = AboutUsActivity.class;
                break;
            case 5:
                activity = PhotoTourActivity.class;
                break;
            case 6:
                activity = TrainersActivity.class;
                break;
            case 7:
                activity = ContactsActivity.class;
                break;
            default:
                activity = MainActivity.class;
        }

        Intent intent = new Intent(this, activity);
        startActivity(intent);
        overridePendingTransition(R.animator.fadein, R.animator.fadeout);
    }

    private void preparePostData() {

        Menu menu = new Menu(
                getResources().getString(R.string.menu_schedule),
                "icon_calendar");
        menuList.add(menu);

        menu = new Menu(
                getResources().getString(R.string.menu_news),
                "icon_world_outline");
        menuList.add(menu);

        menu = new Menu(
                getResources().getString(R.string.menu_services),
                "icon_paper");
        menuList.add(menu);

        menu = new Menu(
                getResources().getString(R.string.menu_notifications),
                "icon_clock");
        menuList.add(menu);

        menu = new Menu(
                getResources().getString(R.string.menu_aboutus),
                "icon_flash");
        menuList.add(menu);

        menu = new Menu(
                getResources().getString(R.string.menu_gallery),
                "icon_camera");
        menuList.add(menu);

        menu = new Menu(
                getResources().getString(R.string.menu_trainer),
                "icon_personadd");
        menuList.add(menu);

        menu = new Menu(
                getResources().getString(R.string.menu_contacts),
                "icon_email_outline");
        menuList.add(menu);

    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, getResources().getString(R.string.exit_app),
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

}
