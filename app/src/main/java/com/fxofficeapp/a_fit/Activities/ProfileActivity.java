package com.fxofficeapp.a_fit.Activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;

public class ProfileActivity extends DrawerActivity {

    int total_bonuses = 0;
    boolean twitterOnce = false;
    boolean facebookOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.personal_account);

        final LinearLayout addFacebookBtn = (LinearLayout)findViewById(R.id.facebookBtn);

        assert addFacebookBtn != null;
        addFacebookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFont facebookIndicator = (CustomFont)findViewById(R.id.facebookIndicator);
                facebookIndicator.setVisibility(View.INVISIBLE);
                CustomFont facebookText = (CustomFont)findViewById(R.id.facebookText);
                facebookText.setText(R.string.connected);
                v.setAlpha((float) 1.0);
                if (facebookOnce != true) addBonuses();
                facebookOnce = true;
            }
        });

        LinearLayout addTwitterBtn = (LinearLayout)findViewById(R.id.twitterBtn);

        assert addTwitterBtn != null;
        addTwitterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFont twitterIndicator = (CustomFont)findViewById(R.id.twitterIndicator);
                twitterIndicator.setVisibility(View.INVISIBLE);
                CustomFont twitterText = (CustomFont)findViewById(R.id.twitterText);
                twitterText.setText(R.string.connected);
                v.setAlpha((float) 1.0);
                if (twitterOnce != true) addBonuses();
                twitterOnce = true;
            }
        });

    }

    public void addBonuses() {

        CustomFont totalBonuses = (CustomFont)findViewById(R.id.totalBonuses);
        total_bonuses = Integer.parseInt(totalBonuses.getText().toString());
        total_bonuses = total_bonuses + 10;
        totalBonuses.setText(String.valueOf(total_bonuses));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.open_checkin) {
//            Intent activityIntent = new Intent(ProfileActivity.this, CheckinActivity.class);
//            ProfileActivity.this.startActivity(activityIntent);
//        }

        return super.onOptionsItemSelected(item);
    }
}
