package com.fxofficeapp.a_fit.Activities;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.App;
import com.fxofficeapp.a_fit.Helper.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Coder on 11/28/16.
 */

public class SplashActivity extends AppCompatActivity {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        String default_app_name = preferences.getString("default_app_name", "");
        String default_app_slug = preferences.getString("default_app_slug", "");

        if(!default_app_name.equalsIgnoreCase("") && !default_app_slug.equalsIgnoreCase("")) {
            Constants.CURRENT_CLUB_NAME = default_app_name;
            Constants.URL_SLUG = default_app_slug;
        }

        final String url = Constants.URL_BASE + Constants.URL_FAMILY;
        RequestQueue queue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        JsonArrayRequest jsonArray = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                final JSONObject jsonData = response.getJSONObject(i);
                                Iterator<String> iterator = jsonData.keys();
                                while(iterator.hasNext()) {
                                    String currentKey = iterator.next();
                                    if (currentKey.equals(Constants.APP_FAMILY)) {

                                        JSONArray family = jsonData.getJSONArray(currentKey);

                                        if (family.length() > 0) {
                                            editor.putString("app_family_array", family.toString());
                                            editor.putString("default_app_name", Constants.CURRENT_CLUB_NAME);
                                            editor.putString("default_app_slug", Constants.URL_SLUG);
                                            editor.commit();
                                            Constants.APP_FAMILY_ARRAY = family;

                                            Intent intent = new Intent(App.getContext(), MainActivity.class);
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                            startActivity(intent);
                                            finish();
                                        }

                                    }

                                }

                            } catch (JSONException e) {

                                Intent intent = new Intent(App.getContext(), MainActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                                finish();
                            }
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Intent intent = new Intent(App.getContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

        // Add the request to the RequestQueue.
        queue.add(jsonArray);


    }
}
