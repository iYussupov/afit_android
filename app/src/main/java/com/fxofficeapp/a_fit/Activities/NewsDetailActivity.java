package com.fxofficeapp.a_fit.Activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.fxofficeapp.a_fit.Helper.App;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.CustomScrollView;
import com.fxofficeapp.a_fit.Helper.CustomTextView;
import com.fxofficeapp.a_fit.Models.Post;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.facebook.FacebookSdk;
import com.fxofficeapp.a_fit.Slideshow.image.SmartImageView;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;
import com.vk.sdk.dialogs.VKShareDialog;
import com.vk.sdk.dialogs.VKShareDialogBuilder;

import me.everything.android.ui.overscroll.IOverScrollDecor;
import me.everything.android.ui.overscroll.IOverScrollUpdateListener;
import me.everything.android.ui.overscroll.VerticalOverScrollBounceEffectDecorator;
import me.everything.android.ui.overscroll.adapters.ScrollViewOverScrollDecorAdapter;


public class NewsDetailActivity extends Fragment {

    private Post post;
    ShareDialog shareDialog;
    boolean shareTrigger = false;
    SmartImageView postThumb;
    float initialX;
    float initialY;
    final int MAX_DISTANCE = 50;

    public NewsDetailActivity(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_news_detail, container, false);

        RelativeLayout layoutWrapper = (RelativeLayout) v.findViewById(R.id.layoutWrapper);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            layoutWrapper.setPadding(0, 0, 0, 0);
        }

        //Share Facebook Button
        final Button facebookShareBtn = (Button) v.findViewById(R.id.facebook_share_btn);

        //Share VK Button
        final Button vkShareBtn = (Button) v.findViewById(R.id.vk_share_btn);

        // Facebook init
        FacebookSdk.sdkInitialize(getActivity());
        AppEventsLogger.activateApp(App.getApplication());
        shareDialog = new ShareDialog(this);

        //Toolbar
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
        //Getting Post Data
        post  = (Post) getArguments().getSerializable("post");

        if (post != null) {

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
            ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.titleview_details);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);

            CustomFont title = (CustomFont) v.findViewById(R.id.appbar_title);
            title.setText(post.getTitle().toUpperCase());

            CustomFont postTitle = (CustomFont) v.findViewById(R.id.post_detail_title);
            CustomFont postDate = (CustomFont) v.findViewById(R.id.post_detail_date);
            final CustomFont postCategory = (CustomFont) v.findViewById(R.id.post_detail_category);
            CustomTextView postDetailText = (CustomTextView) v.findViewById(R.id.post_detail_text);

            postTitle.setText(post.getTitle().toUpperCase());
            postDate.setText(post.getDate());
            postCategory.setText(post.getCategory().toUpperCase());
            postDetailText.setText(post.getContent());

            String img_uri = post.getImageUrl();
            postThumb = (SmartImageView) v.findViewById(R.id.post_detail_thumb);
            postThumb.setReusableHeight(getActivity());

            if (img_uri != "") {
                postThumb.setImageUrl(img_uri);
            }

            postThumb.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        initialX = event.getX();
                        initialY = event.getY();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        float finalX = event.getX();
                        float finalY = event.getY();
                        float deltaX = finalX - initialX;
                        float deltaY = finalY - initialY;
                        if (Math.abs(deltaX) < MAX_DISTANCE && Math.abs(deltaY) < MAX_DISTANCE) {
                            String image_uri = post.getImageUrl();
                            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                            Bundle bundle = new Bundle();
                            bundle.putString("image_uri", image_uri);
                            ImageViewerActivity f = new ImageViewerActivity();
                            f.setArguments(bundle);
                            ft.setCustomAnimations(R.animator.alpha_in, R.animator.alpha_out,R.animator.alpha_in, R.animator.alpha_out);
                            ft.add(android.R.id.content, f);
                            ft.addToBackStack("post");
                            ft.commit();
                        }
                    }
                    return true;
                }
            });


            final CustomScrollView scrollView = (CustomScrollView)v.findViewById(R.id.main_scroll);
            VerticalOverScrollBounceEffectDecorator decor = new VerticalOverScrollBounceEffectDecorator(new ScrollViewOverScrollDecorAdapter(scrollView));

            decor.setOverScrollUpdateListener(new IOverScrollUpdateListener() {
                @Override
                public void onOverScrollUpdate(IOverScrollDecor decor, int state, float offset) {

                    if (offset > 0) {

                        float headerScaleFactor = offset / postThumb.getHeight();
                        float headerSizeVariation = (float) (((postThumb.getHeight() * (1.0 + headerScaleFactor)) - postThumb.getHeight())/2.0);
                        
                        postThumb.setScaleX(1.0f + headerScaleFactor);
                        postThumb.setScaleY(1.0f + headerScaleFactor);
                        postThumb.setTranslationY(-headerSizeVariation);
                        postCategory.setTranslationY(-offset);

                    }
                }
            });

        }

        //Share Button
        final Button detailShareBtn = (Button)v.findViewById(R.id.detail_share_btn);
        assert detailShareBtn != null;
        detailShareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("SHARE","clicked");

            final float scale = getResources().getDisplayMetrics().density;
            int facebookOffset = (int) (109f * scale + 0.5f);
            int vkOffset = (int) (56f * scale + 0.5f);

            if (shareTrigger == false) {

                final Animation shareBtnIn = AnimationUtils.loadAnimation(getActivity(), R.animator.rotate_in);
                shareBtnIn.reset();
                shareBtnIn.setFillAfter(true);
                detailShareBtn.clearAnimation();
                detailShareBtn.startAnimation(shareBtnIn);

                ObjectAnimator facebookBtnIn = ObjectAnimator.ofFloat(facebookShareBtn, "translationX", 0f, -facebookOffset);
                facebookBtnIn.setDuration(250);
                facebookBtnIn.setInterpolator(new AccelerateDecelerateInterpolator());
                facebookBtnIn.start();

                ObjectAnimator vkBtnIn = ObjectAnimator.ofFloat(vkShareBtn, "translationX", 0f, -vkOffset);
                vkBtnIn.setStartDelay(50);
                vkBtnIn.setDuration(250);
                vkBtnIn.setInterpolator(new AccelerateDecelerateInterpolator());
                vkBtnIn.start();

                shareTrigger = true;

            } else {

                final Animation shareBtnOut = AnimationUtils.loadAnimation(getActivity(), R.animator.rotate_out);
                shareBtnOut.reset();
                shareBtnOut.setFillAfter(true);
                detailShareBtn.clearAnimation();
                detailShareBtn.startAnimation(shareBtnOut);

                ObjectAnimator facebookBtnOut = ObjectAnimator.ofFloat(facebookShareBtn, "translationX", -facebookOffset, 0f);
                facebookBtnOut.setDuration(250);
                facebookBtnOut.setStartDelay(50);
                facebookBtnOut.setInterpolator(new AccelerateDecelerateInterpolator());
                facebookBtnOut.start();

                ObjectAnimator vkBtnOut = ObjectAnimator.ofFloat(vkShareBtn, "translationX", -vkOffset, 0f);
                vkBtnOut.setDuration(250);
                vkBtnOut.setInterpolator(new AccelerateDecelerateInterpolator());
                vkBtnOut.start();

                shareTrigger = false;

            }

            }
        });


        facebookShareBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP: {
                        // Do whatever you want here.
                        Log.v("Facebook","clicked");
                        facebookPublish();
                    }
                    return true;
                }
                return false;
            }
        });

        //Share Facebook
        assert facebookShareBtn != null;
        facebookShareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("Facebook","clicked");
                facebookPublish();

            }
        });

        //Share VK
        assert vkShareBtn != null;
        vkShareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("VK","clicked");
                vkontaktePublish();

            }
        });


        return v;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.findItem(R.id.open_contact);
        item.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getFragmentManager().popBackStack();
            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.animator.slide_left, R.animator.slide_right);
            ft.remove(this).commit();

            View contentFrame = getActivity().findViewById(R.id.content_frame);

            ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                    PropertyValuesHolder.ofFloat("translationX", -300.0f, 0f)
            );

            translateX.setDuration(200);
            translateX.setInterpolator(new AccelerateDecelerateInterpolator());
            translateX.start();
        }
        return super.onOptionsItemSelected(item);
    }

    public final void facebookPublish(){

                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(getResources().getString(R.string.social_share_url)))
                        .setContentDescription(post.getContent())
                        .setContentTitle(post.getTitle())
                        .setImageUrl(Uri.parse(post.getImageUrl()))
                        .build();

                shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
    }

    public final void vkontaktePublish() {

        VKAccessToken curToken = VKAccessToken.currentToken();
        if (curToken == null)  {
            VKSdk.login(this, Constants.SCOPE);
                 Toast.makeText(getActivity(), getResources().getString(R.string.vk_post_auth), Toast.LENGTH_LONG).show();
        } else {
                Bitmap currentLoadedImage;
                SmartImageView currentImageView = postThumb;
                currentLoadedImage = ((BitmapDrawable)currentImageView.getDrawable()).getBitmap();


                new VKShareDialogBuilder()

                        .setText(post.getTitle()+"\n"+post.getContent())
                        .setAttachmentImages(new VKUploadImage[]{
                                new VKUploadImage(currentLoadedImage, VKImageParameters.pngImage())
                        })
                        .setAttachmentLink("", getResources().getString(R.string.social_share_url))
                        .setShareDialogListener(new VKShareDialog.VKShareDialogListener() {
                            @Override
                            public void onVkShareComplete(int postId) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.vk_post_done), Toast.LENGTH_LONG).show();

                            }

                            @Override
                            public void onVkShareCancel() {
                                Toast.makeText(getActivity(), getResources().getString(R.string.vk_post_cancel), Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onVkShareError(VKError error) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.vk_post_error), Toast.LENGTH_LONG).show();
                            }
                        })
                        .show(getFragmentManager(), "VK_SHARE_DIALOG");
        }
    }

}
