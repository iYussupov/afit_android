package com.fxofficeapp.a_fit.Activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.app.Fragment;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.App;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.CustomScrollView;
import com.fxofficeapp.a_fit.Helper.CustomTextView;
import com.fxofficeapp.a_fit.Models.Services;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.fxofficeapp.a_fit.Slideshow.image.SmartImageView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;

import me.everything.android.ui.overscroll.IOverScrollDecor;
import me.everything.android.ui.overscroll.IOverScrollUpdateListener;
import me.everything.android.ui.overscroll.VerticalOverScrollBounceEffectDecorator;
import me.everything.android.ui.overscroll.adapters.ScrollViewOverScrollDecorAdapter;

public class ServiceDetailActivity extends Fragment {

    Services service;
    String signUpDate;
    String signUpTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_service_detail, container, false);

        RelativeLayout layoutWrapper = (RelativeLayout) v.findViewById(R.id.layoutWrapper);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            layoutWrapper.setPadding(0, 0, 0, 0);
        }

        //Toolbar
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);


        //Getting Service Data
        service  = (Services) getArguments().getSerializable("service");
        if (service != null) {

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
            ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.titleview_details);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
            CustomFont title = (CustomFont)v.findViewById(R.id.appbar_title);
            title.setText(service.getTitle().toUpperCase());



            CustomFont postTitle = (CustomFont)v.findViewById(R.id.post_detail_title);

            postTitle.setText(service.getTitle().toUpperCase());

            CustomTextView serviceDetailText = (CustomTextView)v.findViewById(R.id.service_detail_text);
            serviceDetailText.setText(service.getContent());

            final SmartImageView postThumb = (SmartImageView) v.findViewById(R.id.service_detail_thumb);
            postThumb.setImageUrl(service.getThumb());
            postThumb.setReusableHeight(getActivity());

            LinearLayout signUpBtn = (LinearLayout)v.findViewById(R.id.signUpBtn);
            signUpBtn.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {

                    showSignUpModal(v);

                }

            });

            //Image Viewer
            assert postThumb != null;
            postThumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String image_uri = service.getThumb();
                    android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("image_uri", image_uri);
                    ImageViewerActivity f = new ImageViewerActivity();
                    f.setArguments(bundle);
                    ft.setCustomAnimations(R.animator.alpha_in, R.animator.alpha_out,R.animator.alpha_in, R.animator.alpha_out);
                    ft.add(android.R.id.content, f);
                    ft.addToBackStack("post");
                    ft.commit();
                }
            });

            final CustomScrollView scrollView = (CustomScrollView)v.findViewById(R.id.main_scroll);
            VerticalOverScrollBounceEffectDecorator decor = new VerticalOverScrollBounceEffectDecorator(new ScrollViewOverScrollDecorAdapter(scrollView));

            decor.setOverScrollUpdateListener(new IOverScrollUpdateListener() {
                @Override
                public void onOverScrollUpdate(IOverScrollDecor decor, int state, float offset) {

                    if (offset > 0) {

                        float headerScaleFactor = offset / postThumb.getHeight();
                        float headerSizeVariation = (float) (((postThumb.getHeight() * (1.0 + headerScaleFactor)) - postThumb.getHeight())/2.0);

                        postThumb.setScaleX(1.0f + headerScaleFactor);
                        postThumb.setScaleY(1.0f + headerScaleFactor);
                        postThumb.setTranslationY(-headerSizeVariation);

                    }
                }
            });

//            int[] IMAGES = { R.drawable.service_thumb_1};
//
//            ViewPagerSlideShow slideshow = (ViewPagerSlideShow)findViewById(R.id.slideshow);
//            SlideShowAdapter adapter = new SlideShowAdapter(getSupportFragmentManager(),IMAGES);
//            slideshow.setAdapter(adapter);

        }

        return v;
    }

    public void showSignUpModal(View anchorView) {

        final View popupView = getActivity().getLayoutInflater().inflate(R.layout.fragment_service_signup, null);

        final PopupWindow popupWindow = new PopupWindow(popupView,
                RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT);

        com.fxofficeapp.a_fit.Helper.CustomFont cancelBtn = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.cancelBtn);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });


        LinearLayout callDatepicker = (LinearLayout)popupView.findViewById(R.id.set_datepicker);
        final com.fxofficeapp.a_fit.Helper.CustomFont datePickerValue = (com.fxofficeapp.a_fit.Helper.CustomFont)popupView.findViewById(R.id.datepicker_value);

        final Dialog dialog = new Dialog(getActivity());

        callDatepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.setContentView(R.layout.custom_datetimepicker_dialog);
//                dialog.setTitle("Custom Dialog");

                final TimePicker tp = (TimePicker)dialog.findViewById(R.id.timePicker1);
                tp.setIs24HourView(true);
//
                final DatePicker dp = (DatePicker)dialog.findViewById(R.id.datePicker1);
                dp.setMinDate(System.currentTimeMillis() - 1000);

                dialog.show();



                Button datepickerReady = (Button)dialog.findViewById(R.id.datepicker_ready);

                datepickerReady.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int   day  = dp.getDayOfMonth();
                        int   month= dp.getMonth();
                        int   year = dp.getYear();

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, month, day);

                        DateTime dt = new DateTime(calendar.getTime());
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.M.yyyy");
                        signUpDate = fmt.print(dt);

                        int hours = tp.getCurrentHour();
                        int mins = tp.getCurrentMinute();
                        signUpTime = hours+":"+mins;

                        datePickerValue.setText(signUpDate +" "+ signUpTime);

                        dialog.hide();
                    }
                });

                Button datepickerCancel = (Button)dialog.findViewById(R.id.datepicker_cancel);

                datepickerCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.hide();
                    }
                });

            }
        });

        final com.fxofficeapp.a_fit.Helper.CustomFont serviceName = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.service_name);

        serviceName.setText(service.getTitle().toUpperCase());


        LinearLayout sendMailBtn = (LinearLayout) popupView.findViewById(R.id.sendSignupBtn);

        sendMailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.fxofficeapp.a_fit.Helper.CustomEditText client_name = (com.fxofficeapp.a_fit.Helper.CustomEditText) popupView.findViewById(R.id.client_name);
                com.fxofficeapp.a_fit.Helper.CustomEditText client_phone = (com.fxofficeapp.a_fit.Helper.CustomEditText) popupView.findViewById(R.id.client_phone);

                String clientName = client_name.getText().toString();
                String clientPhone = client_phone.getText().toString();
                if (!clientName.matches("") & !clientPhone.matches("") ) {
                    sendMail(popupWindow, clientName, clientPhone, signUpDate, signUpTime, service.getTitle());
                } else {
                    Toast.makeText(App.getContext(), "Пожалуйста, заполните все поля!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);

        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        popupWindow.setAnimationStyle(R.style.popupanim);
        popupWindow.showAtLocation(getActivity().getWindow().getDecorView(), Gravity.NO_GRAVITY, 0, 0);

    }

    private void sendMail(final PopupWindow popupWindow, String clientName, String clientPhone, String signUpDate, String signUpTime, final String serviceName) {
        Log.i("Sending email","Now");

        RequestQueue queue = Volley.newRequestQueue(App.getContext());
        String client_name = Uri.encode(clientName);
        String client_phone = Uri.encode(clientPhone);
        String service_name = Uri.encode(serviceName);
        String training_time = Uri.encode(signUpTime);
        String training_date = Uri.encode(signUpDate);
        String token = Constants.DEVICE_TOKEN;

        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_MAILER + "?action=service&device=android&token="+token+"&client_phone="+ client_phone +"&client_name="+ client_name +"&service_name="+ service_name +"&training_time="+ training_time +"&training_date="+ training_date;

        // Request a string response from the provided URL.
        StringRequest mailSendRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(App.getContext(), "Ваша заявка принята и будет обработана в ближайшее время.", Toast.LENGTH_SHORT).show();

                // valid response
                Log.v("RES", response.toString());
                popupWindow.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
            }
        });
        // Add the request to the RequestQueue.
        queue.add(mailSendRequest);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.findItem(R.id.open_contact);
        item.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getFragmentManager().popBackStack();
            View contentFrame = getActivity().findViewById(R.id.content_frame);
            ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                    PropertyValuesHolder.ofFloat("translationX", -300.0f, 0f)
            );

            translateX.setDuration(200);
            translateX.setInterpolator(new AccelerateDecelerateInterpolator());
            translateX.start();
            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.animator.slide_left, R.animator.slide_right);
            ft.remove(this).commit();
        }
        return super.onOptionsItemSelected(item);
    }
}
