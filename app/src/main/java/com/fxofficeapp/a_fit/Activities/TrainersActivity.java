package com.fxofficeapp.a_fit.Activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.TrainersGridAdapter;
import com.fxofficeapp.a_fit.Models.Trainers;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TrainersActivity extends DrawerActivity {

    private List<Trainers> trainersList = new ArrayList<>();
    private TrainersGridAdapter gAdapter;
    private Trainers trainer_data;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public boolean viewInitialized = false;
    private GridView gridview;
    LinearLayout contentFrame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainers);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.trainers);

        contentFrame = (LinearLayout)findViewById(R.id.content_frame);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int size = dm.widthPixels / 2 - 42;

        gridview = (GridView) findViewById(R.id.gridview);
        gAdapter = new TrainersGridAdapter(this, trainersList, size);

        gridview.setAdapter(gAdapter);

        gridview.setOnItemClickListener(gridviewOnItemClickListener);

        parseData();
        checkTimeStamp();
    }

    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {
            Trainers trainer = trainersList.get(position);

            ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                    PropertyValuesHolder.ofFloat("translationX", 0f, -300.0f)
            );

            translateX.setDuration(200);
            translateX.setStartDelay(100);
            translateX.setInterpolator(new AccelerateDecelerateInterpolator());
            translateX.start();

            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putSerializable("trainer", trainer);
            TrainersDetailActivity f = new TrainersDetailActivity();
            f.setArguments(bundle);
            ft.setCustomAnimations(R.animator.slide_left, R.animator.slide_right,R.animator.slide_left, R.animator.slide_right);
            ft.add(R.id.fragment_wrapper, f);
            ft.addToBackStack("trainer");
            ft.commit();
        }
    };

    void checkTimeStamp() {
        String trainers_data = preferences.getString("trainers_data_"+Constants.URL_SLUG, "");
        if(!trainers_data.equalsIgnoreCase(""))
        {
            try {

                JSONObject obj = new JSONObject(trainers_data);
                final int timeStampCached = obj.getInt("time");

                // Instantiate the RequestQueue.
                String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_TRAINERS;
                RequestQueue queue = Volley.newRequestQueue(this);

                Log.v("cache", "checking timestamp");

                // Request a string response from the provided URL.
                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                try {

                                    int timeStamp = response.getInt("time");

                                    if (timeStampCached != timeStamp) {

                                        parseFreshData();
                                        Log.v("cache", "refresh data");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });

                // Add the request to the RequestQueue.
                queue.add(jsObjRequest);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void parseData() {

        String trainers_data = preferences.getString("trainers_data_"+Constants.URL_SLUG, "");
        if(!trainers_data.equalsIgnoreCase(""))
        {

            Log.v("cache parse", "we are here");
            try {

                JSONObject obj = new JSONObject(trainers_data);
                updateModel(obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            parseFreshData();

        }

    }

    void parseFreshData(){

        // Instantiate the RequestQueue.
        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_TRAINERS;
        RequestQueue queue = Volley.newRequestQueue(this);

        // Request a string response from the provided URL.
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        editor.putString("trainers_data_"+Constants.URL_SLUG,response.toString());
                        editor.commit();
                        updateModel(response);

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
                    }
                });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);
    }

    void updateModel(JSONObject response){
        try {

            int timeStamp = response.getInt("time");
            JSONArray trainers = response.getJSONArray("trainers");
            trainersList.clear();

            for (int i = 0; i < trainers.length(); i++) {
                final JSONObject jsonData = trainers.getJSONObject(i);

                String title = "";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    title = Html.fromHtml(jsonData.getString("name"),Html.FROM_HTML_MODE_LEGACY).toString();
                } else {
                    title = Html.fromHtml(jsonData.getString("name")).toString();
                }

                String content = "";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    content = Html.fromHtml(jsonData.getString("content"),Html.FROM_HTML_MODE_LEGACY).toString();
                } else {
                    content = Html.fromHtml(jsonData.getString("content")).toString();
                }
                String category = jsonData.getString("ocupation");
                String images = jsonData.getString("image_url");

                trainer_data = new Trainers(timeStamp, title, images, category, content);
                trainersList.add(trainer_data);

                gAdapter.notifyDataSetChanged();
                gridview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        viewInitialized = true;
                    }
                });

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View decorView = this.getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
        decorView.setSystemUiVisibility(uiOptions);
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
            ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                    PropertyValuesHolder.ofFloat("translationX", -300.0f, 0f)
            );

            translateX.setDuration(200);
            translateX.setInterpolator(new AccelerateDecelerateInterpolator());
            translateX.start();
        }

    }

}
