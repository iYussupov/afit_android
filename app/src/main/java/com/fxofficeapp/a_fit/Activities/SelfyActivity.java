package com.fxofficeapp.a_fit.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.fxofficeapp.a_fit.Helper.SelfyGridAdapter;
import com.fxofficeapp.a_fit.Models.Selfy;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import java.util.ArrayList;
import java.util.List;


public class SelfyActivity extends DrawerActivity {

    private List<Selfy> selfyList = new ArrayList<>();
    private SelfyGridAdapter selfyAdapter;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selfy);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.our_family);

        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int size = dm.widthPixels / 2 - 42;

        GridView gridview = (GridView) findViewById(R.id.gridview);
        selfyAdapter = new SelfyGridAdapter(this, selfyList, size);

        if (!(selfyList.size() > 0)) {
            preparePostData();
        }

        gridview.setAdapter(selfyAdapter);

        gridview.setOnItemClickListener(gridviewOnItemClickListener);
    }

    private GridView.OnItemClickListener gridviewOnItemClickListener = new GridView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position,
                                long id) {
            Selfy selfy = selfyList.get(position);
            Intent activityIntent = new Intent(SelfyActivity.this, SelfyDetailActivity.class);
            activityIntent.putExtra("selfy", selfy);
            SelfyActivity.this.startActivity(activityIntent);
        }
    };


    private void preparePostData() {

        Selfy selfy = new Selfy("BILL BROOKS", "service_thumb_1", "COMBAT SPORTS", 75, 12, true);
        selfyList.add(selfy);

        selfy = new Selfy("ALENA BORODINA", "service_thumb_2", "COMBAT SPORTS", 145, 4, false);
        selfyList.add(selfy);

        selfy = new Selfy("ALENA BORODINA", "service_thumb_3", "COMBAT SPORTS", 98, 7, false);
        selfyList.add(selfy);

        selfy = new Selfy("ALENA BORODINA", "service_thumb_4", "COMBAT SPORTS", 43, 12, true);
        selfyList.add(selfy);

        selfy = new Selfy("ALENA BORODINA", "service_thumb_5", "COMBAT SPORTS", 145, 4, false);
        selfyList.add(selfy);

        selfy = new Selfy("ALENA BORODINA", "service_thumb_6", "COMBAT SPORTS", 98, 7, false);
        selfyList.add(selfy);

    }
}
