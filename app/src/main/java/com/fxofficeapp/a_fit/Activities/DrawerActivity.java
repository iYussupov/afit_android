package com.fxofficeapp.a_fit.Activities;

import android.app.ActionBar;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.LayoutRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.fxofficeapp.a_fit.Helper.App;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.fxofficeapp.a_fit.Helper.ShowPopUp;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.MenuAdapter;

import java.util.List;

import static com.fxofficeapp.a_fit.Helper.App.getContext;

public class DrawerActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter<String> listAdapter;
    private MenuItem menuItem_;

    public static boolean hideOptionsMenu = false;

    protected void onCreateDrawer(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
        getSupportActionBar().setCustomView(R.layout.titleview);
        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);

        appTitle.setText(R.string.app_name);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        listView = (ListView)findViewById(R.id.menuItemsList);
        listAdapter = new MenuAdapter(this, getResources().getStringArray(R.array.menuEntries));
        listView.setAdapter(listAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                Class activity;
                switch (position){
                    case 0:
                        activity = MainActivity.class;
                        break;
                    case 1:
                        activity = ScheduleActivity.class;
                        break;
                    case 2:
                        activity = NewsActivity.class;
                        break;
                    case 3:
                        activity = PushActivity.class;
                        break;
                    case 4:
                        activity = AboutUsActivity.class;
                        break;
                    case 5:
                        activity = PhotoTourActivity.class;
                        break;
                    case 6:
                        activity = ServicesActivity.class;
                        break;
                    case 7:
                        activity = TrainersActivity.class;
                        break;
                    case 8:
                        activity = ContactsActivity.class;
                        break;
                    default:
                        activity = MainActivity.class;
                        break;
                }

                ActivityManager am = (ActivityManager) getContext().getSystemService(ACTIVITY_SERVICE);
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                String currentActivity = taskInfo.get(0).topActivity.getClassName();

                if (!activity.getCanonicalName().equals(currentActivity)) {

                    Intent activityIntent = new Intent(DrawerActivity.this, activity);
                    if (activity == MainActivity.class) {
                        activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        DrawerActivity.this.startActivity(activityIntent);
                        finish();
                    } else {
                        DrawerActivity.this.startActivity(activityIntent);
                    }

                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
                drawer.closeDrawer(GravityCompat.START);

            }
        });

    }

    @Override
    public void onBackPressed() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View decorView = this.getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
        decorView.setSystemUiVisibility(uiOptions);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
            Intent activityIntent = new Intent(DrawerActivity.this, MainActivity.class);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            DrawerActivity.this.startActivity(activityIntent);
            overridePendingTransition(R.animator.fadein, R.animator.fadeout);
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID)
    {
        super.setContentView(layoutResID);
        onCreateDrawer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.open_contact);
        menuItem_ = item;
        item.setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.open_contact) {
            Intent activityIntent = new Intent(DrawerActivity.this, ContactsActivity.class);
            DrawerActivity.this.startActivity(activityIntent);
        }

        if (id == R.id.open_filter) {
            View v = this.getWindow().getDecorView().findViewById(android.R.id.content);
            new ShowPopUp(App.getContext()).trainingsFilter(v, this);
        }

        return super.onOptionsItemSelected(item);
    }

}
