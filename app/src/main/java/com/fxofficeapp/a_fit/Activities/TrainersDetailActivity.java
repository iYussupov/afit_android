package com.fxofficeapp.a_fit.Activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.app.Fragment;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.App;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.CustomScrollView;
import com.fxofficeapp.a_fit.Models.Trainers;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.fxofficeapp.a_fit.Helper.CustomTextView;
import com.fxofficeapp.a_fit.Slideshow.image.SmartImageView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;

import me.everything.android.ui.overscroll.IOverScrollDecor;
import me.everything.android.ui.overscroll.IOverScrollUpdateListener;
import me.everything.android.ui.overscroll.VerticalOverScrollBounceEffectDecorator;
import me.everything.android.ui.overscroll.adapters.ScrollViewOverScrollDecorAdapter;

public class TrainersDetailActivity extends Fragment {

    Trainers trainer;
    String signUpDate;
    String signUpTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_trainers_detail, container, false);

        RelativeLayout layoutWrapper = (RelativeLayout) v.findViewById(R.id.layoutWrapper);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            layoutWrapper.setPadding(0, 0, 0, 0);
        }

        //Toolbar
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        //Getting Post Data
        trainer  = (Trainers) getArguments().getSerializable("trainer");
        if (trainer != null) {

            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
            ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.titleview_details);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
            CustomFont appTitle = (CustomFont)v.findViewById(R.id.appbar_title);
            appTitle.setText(trainer.getName().toUpperCase());

            CustomFont postTitle = (CustomFont)v.findViewById(R.id.post_detail_title);
            CustomTextView postContent = (CustomTextView)v.findViewById(R.id.post_detail_text);

            postTitle.setText(trainer.getName().toUpperCase());
            postContent.setText(trainer.getContent());

            final SmartImageView postThumb = (SmartImageView) v.findViewById(R.id.post_detail_thumb);
            postThumb.setReusableHeight(getActivity());
            String img_uri = trainer.getFeaturedImg();
            postThumb.setImageUrl(img_uri);

            LinearLayout signUpBtn = (LinearLayout)v.findViewById(R.id.signUpBtn);
            signUpBtn.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {

                    showSignUpModal(v);

                }

            });

            //Image Viewer
            assert postThumb != null;
            postThumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String image_uri = trainer.getFeaturedImg();
                    android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("image_uri", image_uri);
                    ImageViewerActivity f = new ImageViewerActivity();
                    f.setArguments(bundle);
                    ft.setCustomAnimations(R.animator.alpha_in, R.animator.alpha_out,R.animator.alpha_in, R.animator.alpha_out);
                    ft.add(android.R.id.content, f);
                    ft.addToBackStack("post");
                    ft.commit();
                }
            });

            final CustomScrollView scrollView = (CustomScrollView)v.findViewById(R.id.main_scroll);
            VerticalOverScrollBounceEffectDecorator decor = new VerticalOverScrollBounceEffectDecorator(new ScrollViewOverScrollDecorAdapter(scrollView));

            decor.setOverScrollUpdateListener(new IOverScrollUpdateListener() {
                @Override
                public void onOverScrollUpdate(IOverScrollDecor decor, int state, float offset) {

                    if (offset > 0) {

                        float headerScaleFactor = offset / postThumb.getHeight();
                        float headerSizeVariation = (float) (((postThumb.getHeight() * (1.0 + headerScaleFactor)) - postThumb.getHeight())/2.0);

                        postThumb.setScaleX(1.0f + headerScaleFactor);
                        postThumb.setScaleY(1.0f + headerScaleFactor);
                        postThumb.setTranslationY(-headerSizeVariation);

                    }
                }
            });

        }

        return v;
    }

    public void showSignUpModal(View anchorView) {

        final View popupView = getActivity().getLayoutInflater().inflate(R.layout.fragment_trainer_signup, null);

        final PopupWindow popupWindow = new PopupWindow(popupView,
                RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT);

        com.fxofficeapp.a_fit.Helper.CustomFont cancelBtn = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.cancelBtn);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });


        LinearLayout callDatepicker = (LinearLayout)popupView.findViewById(R.id.set_datepicker);
        final com.fxofficeapp.a_fit.Helper.CustomFont datePickerValue = (com.fxofficeapp.a_fit.Helper.CustomFont)popupView.findViewById(R.id.datepicker_value);

        final Dialog dialog = new Dialog(getActivity());

        callDatepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.setContentView(R.layout.custom_datetimepicker_dialog);
//                dialog.setTitle("Custom Dialog");

                final TimePicker tp = (TimePicker)dialog.findViewById(R.id.timePicker1);
                tp.setIs24HourView(true);
//
                final DatePicker dp = (DatePicker)dialog.findViewById(R.id.datePicker1);
                dp.setMinDate(System.currentTimeMillis() - 1000);

                dialog.show();


                Button datepickerReady = (Button)dialog.findViewById(R.id.datepicker_ready);

                datepickerReady.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int   day  = dp.getDayOfMonth();
                        int   month= dp.getMonth();
                        int   year = dp.getYear();

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, month, day);

                        DateTime dt = new DateTime(calendar.getTime());
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.M.yyyy");
                        signUpDate = fmt.print(dt);

                        int hours = tp.getCurrentHour();
                        int mins = tp.getCurrentMinute();
                        signUpTime = hours+":"+mins;

                        datePickerValue.setText(signUpDate +" "+ signUpTime);

                        dialog.hide();
                    }
                });

                Button datepickerCancel = (Button)dialog.findViewById(R.id.datepicker_cancel);

                datepickerCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.hide();
                    }
                });

            }
        });

        final com.fxofficeapp.a_fit.Helper.CustomFont trainingName = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.trainer_name);

        trainingName.setText(trainer.getName().toUpperCase());


        LinearLayout sendMailBtn = (LinearLayout) popupView.findViewById(R.id.sendSignupBtn);

        sendMailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.fxofficeapp.a_fit.Helper.CustomEditText client_name = (com.fxofficeapp.a_fit.Helper.CustomEditText) popupView.findViewById(R.id.client_name);
                com.fxofficeapp.a_fit.Helper.CustomEditText client_phone = (com.fxofficeapp.a_fit.Helper.CustomEditText) popupView.findViewById(R.id.client_phone);

                String clientName = client_name.getText().toString();
                String clientPhone = client_phone.getText().toString();
                if (!clientName.matches("") & !clientPhone.matches("") ) {
                    sendMail(popupWindow, clientName, clientPhone, signUpDate, signUpTime, trainer.getName());
                } else {
                    Toast.makeText(App.getContext(), "Пожалуйста, заполните все поля!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);

        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
//        popupView.setAnimation(AnimationUtils.loadAnimation(this, R.animator.fadein));
        popupWindow.setAnimationStyle(R.style.popupanim);
        popupWindow.showAtLocation(getActivity().getWindow().getDecorView(), Gravity.NO_GRAVITY, 0, 0);

    }

    private void sendMail(final PopupWindow popupWindow, String clientName, String clientPhone, String signUpDate, String signUpTime, final String trainerName) {
        Log.i("Sending email","Now");

        RequestQueue queue = Volley.newRequestQueue(App.getContext());
        String client_name = Uri.encode(clientName);
        String client_phone = Uri.encode(clientPhone);
        String training_name = Uri.encode(trainerName);
        String training_time = Uri.encode(signUpTime);
        String training_date = Uri.encode(signUpDate);
        String token = Constants.DEVICE_TOKEN;

        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_MAILER + "?action=trainer&device=android&token="+token+"&client_phone="+ client_phone +"&client_name="+ client_name +"&trainer_name="+ training_name +"&training_time="+ training_time +"&training_date="+ training_date;

        // Request a string response from the provided URL.
        StringRequest mailSendRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(App.getContext(), "Ваша заявка принята и будет обработана в ближайшее время.", Toast.LENGTH_SHORT).show();

                // valid response
                Log.v("RES", response.toString());
                popupWindow.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error
            }
        });
        // Add the request to the RequestQueue.
        queue.add(mailSendRequest);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.findItem(R.id.open_contact);
        item.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getFragmentManager().popBackStack();
            View contentFrame = getActivity().findViewById(R.id.content_frame);
            ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                    PropertyValuesHolder.ofFloat("translationX", -300.0f, 0f)
            );

            translateX.setDuration(200);
            translateX.setInterpolator(new AccelerateDecelerateInterpolator());
            translateX.start();
            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.animator.slide_left, R.animator.slide_right);
            ft.remove(this).commit();
        }
        return super.onOptionsItemSelected(item);
    }
}
