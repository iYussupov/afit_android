package com.fxofficeapp.a_fit.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.fxofficeapp.a_fit.R;

import java.io.FileDescriptor;
import java.io.IOException;

public class CheckinActivity extends DrawerActivity {

    private static final int SELECT_PICTURE = 1;
    private String selectedImagePath;
    private String filemanagerstring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.check_in);

        LinearLayout sendCheckInBtn = (LinearLayout) findViewById(R.id.sendCheckInBtn);

        assert sendCheckInBtn != null;
        sendCheckInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(CheckinActivity.this, MainActivity.class);
                CheckinActivity.this.startActivity(activityIntent);
            }
        });

        RelativeLayout photoViewBtn = (RelativeLayout)findViewById(R.id.photoView);

        assert photoViewBtn != null;
        photoViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE);

            }
        });

    }

    //UPDATED
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();

                //MEDIA GALLERY
                selectedImagePath = getPath(selectedImageUri);

                ImageView imageView = (ImageView) findViewById(R.id.setImageView);

                //NOW WE HAVE OUR WANTED STRING
                if(selectedImagePath!=null) {
                    imageView.setImageBitmap(BitmapFactory.decodeFile(selectedImagePath));
                } else {
                    try {
                        imageView.setImageBitmap(getBitmapFromUri(selectedImageUri));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            }
        }
    }

    //UPDATED!
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if(cursor!=null)
        {
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        else return null;
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }
}
