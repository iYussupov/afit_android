package com.fxofficeapp.a_fit.Activities;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.CustomFont;
import com.fxofficeapp.a_fit.Helper.PushListAdapter;
import com.fxofficeapp.a_fit.Models.Post;
import com.fxofficeapp.a_fit.Models.Push;
import com.fxofficeapp.a_fit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PushActivity extends DrawerActivity {

    private List<Push> pushList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PushListAdapter mAdapter;
    private Push push_data;
    private LinearLayoutManager mLayoutManager;
    private Post post;
    public boolean viewInitialized = false;
    LinearLayout contentFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push);

        CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
        appTitle.setText(R.string.push_messages);

        contentFrame = (LinearLayout)findViewById(R.id.content_frame);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            Post post_type = (Post) bundle.getSerializable("push_received");
            if (post_type != null) {

                MainActivity m = new MainActivity();
                m.startedFlag = true;

                ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                        PropertyValuesHolder.ofFloat("translationX", 0f, -300.0f)
                );

                translateX.setDuration(200);
                translateX.setStartDelay(100);
                translateX.setInterpolator(new AccelerateDecelerateInterpolator());
                translateX.start();

                android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                Bundle newBundle = new Bundle();
                newBundle.putSerializable("post", post_type);
                NewsDetailActivity f = new NewsDetailActivity();
                f.setArguments(newBundle);
                ft.setCustomAnimations(R.animator.slide_left, R.animator.slide_right, R.animator.slide_left, R.animator.slide_right);
                ft.add(R.id.fragment_wrapper, f);
                ft.addToBackStack("post");
                ft.commit();
            }
        }

        recyclerView = (RecyclerView)findViewById(R.id.pushRecyclerView);

        mAdapter = new PushListAdapter(pushList);
        mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(false);

        parseFreshData();
    }

    void parseFreshData(){

        final RequestQueue queue = Volley.newRequestQueue(this);
        String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_PUSH;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        updateModel(response);
                        mAdapter.notifyDataSetChanged();
                        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                viewInitialized = true;
                            }
                        });

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
                    }
                });
        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);
    }

    void updateModel(JSONObject response){

        try {
            JSONArray obj = response.getJSONArray("result");

            for (int i = 0; i < obj.length(); i++) {

                JSONObject jsonData = obj.getJSONObject(i);

                String message = jsonData.getString("message");
                int post_id = jsonData.getInt("post_id");

                String date = "";

                String dtStart = jsonData.getString("starttime");
                try {

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    Date newDate = format.parse(dtStart);
                    format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                    date = format.format(newDate);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                push_data = new Push(message, date, post_id);
                pushList.add(push_data);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void openPostDetails(View anchorView, int postID) {

            // Instantiate the RequestQueue.
            final RequestQueue queue = Volley.newRequestQueue(this);
            String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_POSTS + "/" + postID;

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            try {

                                JSONObject jsonData = response;

                                String category = jsonData.getString("post_category_name");

                                String postTitle = "";
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    postTitle = Html.fromHtml(jsonData.getJSONObject("title").getString("rendered"),Html.FROM_HTML_MODE_LEGACY).toString();
                                } else {
                                    postTitle = Html.fromHtml(jsonData.getJSONObject("title").getString("rendered")).toString();
                                }

                                String content = "";
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    content = Html.fromHtml(jsonData.getJSONObject("content").getString("rendered"),Html.FROM_HTML_MODE_LEGACY).toString();
                                } else {
                                    content = Html.fromHtml(jsonData.getJSONObject("content").getString("rendered")).toString();
                                }

                                String img = jsonData.getString("featured_image_thumbnail_url");
                                String date = "";

                                String dtStart = jsonData.getString("date");
                                try {

                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                    Date newDate = format.parse(dtStart);
                                    format = new SimpleDateFormat("dd.MM.yyyy");
                                    date = format.format(newDate);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                post = new Post(postTitle, date, category, img, content);

                                ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                                        PropertyValuesHolder.ofFloat("translationX", 0f, -300.0f)
                                );

                                translateX.setDuration(200);
                                translateX.setStartDelay(100);
                                translateX.setInterpolator(new AccelerateDecelerateInterpolator());
                                translateX.start();

                                android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("post", post);
                                NewsDetailActivity f = new NewsDetailActivity();
                                f.setArguments(bundle);
                                ft.setCustomAnimations(R.animator.slide_left, R.animator.slide_right,R.animator.slide_left, R.animator.slide_right);
                                ft.add(R.id.fragment_wrapper, f);
                                ft.addToBackStack("post");
                                ft.commit();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
            // Add the request to the RequestQueue.
            queue.add(jsObjRequest);

    }
    @Override
    public void onBackPressed() {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View decorView = this.getWindow().getDecorView();
        // Show the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
        decorView.setSystemUiVisibility(uiOptions);
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();

            ObjectAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(contentFrame,
                    PropertyValuesHolder.ofFloat("translationX", -300.0f, 0f)
            );

            translateX.setDuration(200);
            translateX.setInterpolator(new AccelerateDecelerateInterpolator());
            translateX.start();
        }

    }

}

