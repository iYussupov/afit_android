package com.fxofficeapp.a_fit.Activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.fxofficeapp.a_fit.Models.Selfy;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Helper.CustomFont;

public class SelfyDetailActivity extends AppCompatActivity {

    private Selfy selfy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selfy_detail);

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //Getting Post Data
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            selfy  = (Selfy) extras.getSerializable("selfy");

            CustomFont appTitle = (CustomFont)findViewById(R.id.appbar_title);
            appTitle.setText(selfy.getName().toUpperCase());

//            CustomFont postTitle = (CustomFont)findViewById(R.id.post_detail_title);
//            CustomTextView postContent = (CustomTextView)findViewById(R.id.post_detail_text);
//
//            postTitle.setText(selfy.getName().toUpperCase());
//            postContent.setText(selfy.getContent());

            ImageView postThumb = (ImageView)findViewById(R.id.post_detail_thumb);

            Context context = postThumb.getContext();
            int id = context.getResources().getIdentifier(selfy.getImage_url(), "drawable", context.getPackageName());
            postThumb.setImageResource(id);




        }
    }
}