package com.fxofficeapp.a_fit.Fragments;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.VideoView;

import com.fxofficeapp.a_fit.Activities.MainActivity;
import com.fxofficeapp.a_fit.Helper.App;
import com.fxofficeapp.a_fit.Helper.Constants;
import com.fxofficeapp.a_fit.Helper.CustomVideoView;
import com.fxofficeapp.a_fit.Helper.ShowPopUp;
import com.fxofficeapp.a_fit.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LaunchFragment extends Fragment {


    public LaunchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View decorView = getActivity().getWindow().getDecorView();
        final View v = inflater.inflate(R.layout.fragment_launch, container, false);

        final CustomVideoView videoView = (CustomVideoView) v.findViewById(R.id.videoView);
        videoView.setVideoURI(Uri.parse("android.resource://com.fxofficeapp.a_fit/"  + R.raw.logovideo));

//        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_LOW_PROFILE
//                | View.SYSTEM_UI_FLAG_IMMERSIVE);


        videoView.requestFocus(0);
        videoView.start();

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer arg0) {

                if (Constants.APP_FAMILY_ARRAY.length() > 1)  {

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    final SharedPreferences.Editor editor = preferences.edit();
                    boolean first_start = preferences.getBoolean("first_start", true);
                    if(first_start)
                    {

                        v.post(new Runnable() {
                            public void run() {
                                new ShowPopUp(App.getContext()).clubSelect(v, getActivity());
                                editor.putBoolean("first_start",false);
                                editor.commit();
                            }
                        });

                    }

                }
                decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

                getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentById(android.R.id.content)).commit();

                ((MainActivity)getActivity()).updatePanelsUI();
            }
        });

        return v;
    }

}