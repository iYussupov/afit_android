package com.fxofficeapp.a_fit.Slideshow.image;


import android.content.Context;
import android.graphics.Bitmap;

interface SmartImage {
    public Bitmap getBitmap(Context context);
}