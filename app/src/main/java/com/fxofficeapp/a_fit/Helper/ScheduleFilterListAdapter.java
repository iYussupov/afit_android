package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fxofficeapp.a_fit.R;

import java.util.ArrayList;

/**
 * Created by Dev1 on 12/26/16.
 */

public class ScheduleFilterListAdapter extends RecyclerView.Adapter<ScheduleFilterListAdapter.MyViewHolder> {

    Context context;
    ArrayList<String> classItems;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CustomFont trainingName;
        public ImageView iconUnchecked, iconChecked;

        public MyViewHolder(View view) {

            super(view);
            trainingName = (CustomFont) view.findViewById(R.id.training_name);
            iconUnchecked = (ImageView) view.findViewById(R.id.icon_unchecked);
            iconChecked = (ImageView) view.findViewById(R.id.icon_checked);
        }

        @Override
        public void onClick(View v) {

        }

    }

    public ScheduleFilterListAdapter(ArrayList<String> classItems) {
        this.classItems = classItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_schedule_filter_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        String className = classItems.get(position);

        holder.trainingName.setText(className);

            if (Constants.TRAINING_FILTER_NAME.equals(className)) {

                holder.iconUnchecked.setVisibility(View.GONE);
                holder.iconChecked.setVisibility(View.VISIBLE);

            } else {

                holder.iconUnchecked.setVisibility(View.VISIBLE);
                holder.iconChecked.setVisibility(View.GONE);

            }


    }

    @Override
    public int getItemCount() {
        return classItems.size();
    }
}
