package com.fxofficeapp.a_fit.Helper;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.fxofficeapp.a_fit.Activities.TrainersActivity;
import com.fxofficeapp.a_fit.Models.Trainers;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Slideshow.image.SmartImageView;

import java.util.List;

/**
 * Created by Ingwar on 6/24/16.
 */
public class TrainersGridAdapter extends BaseAdapter {
    private Context mContext;

    private List<Trainers> trainersList;
    private int size;

    public TrainersGridAdapter(Context c, List<Trainers> trainersList, int size) {
        mContext = c;
        this.trainersList = trainersList;
        this.size = size;
    }

    public int getCount() {
        return trainersList.size();
    }

    public Object getItem(int position) {
        return trainersList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View grid;

        if (convertView == null) {
            grid = new View(mContext);

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.trainers_gridcell, parent, false);
        } else {
            grid = (View) convertView;
        }

        SmartImageView imageView = (SmartImageView) grid.findViewById(R.id.imagepart);
        TextView textView = (TextView) grid.findViewById(R.id.textpart);
        TextView textProf = (TextView) grid.findViewById(R.id.textProff);

        Trainers trainer = trainersList.get(position);
        String img_uri = trainer.getFeaturedImg();

        imageView.getLayoutParams().height = size;

        final Context context = imageView.getContext();
        Drawable mask = ResourcesCompat.getDrawable(context.getResources(), R.drawable.mask, null);

        if (!img_uri.isEmpty()) {
            imageView.setImageUrl(img_uri);
        } else {
            imageView.setImageDrawable(mask);
        }


        textView.setText(trainer.getName().toUpperCase());
        textProf.setText(trainer.getCategory().toUpperCase());
        Context mContext = grid.getContext();

        if (!((TrainersActivity)mContext).viewInitialized) {

            grid.setAlpha(0);
            grid.setTranslationY(100f);

            ObjectAnimator gridItemAlpha = ObjectAnimator.ofPropertyValuesHolder(grid,
                    PropertyValuesHolder.ofFloat("translationY", 100f, 0f),
                    PropertyValuesHolder.ofFloat(View.ALPHA, 0.0f, 1.0f)
            );

            gridItemAlpha.setDuration(300);
            gridItemAlpha.setStartDelay((position+1)*125);
            gridItemAlpha.setInterpolator(new DecelerateInterpolator());

            gridItemAlpha.start();

        }

        return grid;
    }

}