package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Created by Dev1 on 2/3/17.
 */

public class CustomScrollView extends ScrollView {

    public CustomScrollView(Context context) {
        super(context);
    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    float initialX;
    float initialY;
    static final int MIN_DISTANCE = 50;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {

        final int action = MotionEventCompat.getActionMasked(event);

        if (action == MotionEvent.ACTION_DOWN) {
            initialX = event.getX();
            initialY = event.getY();
        } else if (action == MotionEvent.ACTION_MOVE) {
            float finalX = event.getX();
            float finalY = event.getY();
            float deltaX = finalX - initialX;
            float deltaY = finalY - initialY;
            if (Math.abs(deltaX) > MIN_DISTANCE || Math.abs(deltaY) > MIN_DISTANCE) {
                return true;
            }
            return false;
        }

        return false;
    }

}
