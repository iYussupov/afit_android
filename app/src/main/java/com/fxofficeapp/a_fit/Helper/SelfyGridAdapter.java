package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.fxofficeapp.a_fit.Models.Selfy;
import com.fxofficeapp.a_fit.R;

import java.util.List;

/**
 * Created by Ingwar on 6/27/16.
 */
public class SelfyGridAdapter extends BaseAdapter {
    private Context mContext;


    private List<Selfy> selfyList;
    private int size;

    public SelfyGridAdapter(Context c, List<Selfy> selfyList, int size) {
        mContext = c;
        this.selfyList = selfyList;
        this.size = size;
    }

    public int getCount() {
        return selfyList.size();
    }

    public Object getItem(int position) {
        return selfyList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        View grid;

        if (convertView == null) {
            grid = new View(mContext);

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.selfy_gridcell, parent, false);
        } else {
            grid = (View) convertView;
        }

        ImageView imageView = (ImageView) grid.findViewById(R.id.imagepart);

        Selfy selfy = selfyList.get(position);

        int id = mContext.getResources().getIdentifier(selfy.getImage_url(), "drawable", mContext.getPackageName());
        imageView.getLayoutParams().height = size;
        imageView.setImageResource(id);

        CustomFont likesCount = (CustomFont) grid.findViewById(R.id.likes_count);
        likesCount.setText(String.valueOf(selfy.getLikes_count()));

        CustomFont commentsCount = (CustomFont) grid.findViewById(R.id.chat_count);
        commentsCount.setText(String.valueOf(selfy.getComments_count()));

        if (selfy.isLikes()){
            CustomIconFont liked = (CustomIconFont) grid.findViewById(R.id.likes);
            liked.setText("\uF141");
            liked.setTextColor(ContextCompat.getColor(mContext, R.color.mainGreen));
        }

        return grid;
    }

}
