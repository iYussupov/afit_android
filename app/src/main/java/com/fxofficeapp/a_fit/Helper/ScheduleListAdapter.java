package com.fxofficeapp.a_fit.Helper;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;

import com.fxofficeapp.a_fit.Activities.ScheduleActivity;
import com.fxofficeapp.a_fit.Helper.anim.ExpandableViewHoldersUtil;
import com.fxofficeapp.a_fit.Models.Schedule;
import com.fxofficeapp.a_fit.R;
import java.util.List;

/**
 * Created by Ingwar on 6/28/16.
 */
public class ScheduleListAdapter extends RecyclerView.Adapter<ScheduleListAdapter.MyViewHolder> {

    private List<Schedule> scheduleList;
    Context context;
    private boolean scheduleInitialized;

    ExpandableViewHoldersUtil.KeepOneH<MyViewHolder> keepOne = new ExpandableViewHoldersUtil.KeepOneH<MyViewHolder>();

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener, ExpandableViewHoldersUtil.Expandable {
        public CustomFont scheduleTitle, postDetailDate, schedulePosition, scheduleDuration;
        public CustomTextView scheduleContent;
        public final LinearLayout infos;
        public LinearLayout trainigSighUpBtn;
        float initialX;
        float initialY;
        static final int MAX_DISTANCE = 50;

        public MyViewHolder(View view) {

            super(view);
            scheduleTitle = (CustomFont) view.findViewById(R.id.schedule_title);
            scheduleContent = (CustomTextView) view.findViewById(R.id.schedule_content);
            postDetailDate = (CustomFont)view
                    .findViewById(R.id.post_detail_date);
            schedulePosition = (CustomFont)view
                    .findViewById(R.id.schedule_position);
            scheduleDuration = (CustomFont)view
                    .findViewById(R.id.schedule_duration);

            infos = (LinearLayout) itemView.findViewById(R.id.schedule_collapse);
            view.setOnTouchListener(this);

            trainigSighUpBtn = (LinearLayout) itemView.findViewById(R.id.trainingSignUpBtn);

        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                initialX = event.getX();
                initialY = event.getY();
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                float finalX = event.getX();
                float finalY = event.getY();
                float deltaX = finalX - initialX;
                float deltaY = finalY - initialY;
                if (Math.abs(deltaX) < MAX_DISTANCE && Math.abs(deltaY) < MAX_DISTANCE) {
                    keepOne.toggle(this);
                }
            }
            return true;
        }

        public void bind(int pos) {
            keepOne.bind(this, pos);
        }

        @Override
        public View getExpandView() {
            return infos;
        }


    }


    public ScheduleListAdapter(List<Schedule> scheduleList, boolean scheduleInitialized) {
        this.scheduleList = scheduleList;
        this.scheduleInitialized = scheduleInitialized;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_schedule_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Schedule schedule = scheduleList.get(position);

        holder.postDetailDate.setText(schedule.getStartTime());
        holder.scheduleContent.setText(schedule.getContent());
        holder.scheduleTitle.setText(schedule.getTitle().toUpperCase());
        if (schedule.getPlace() != ""){
            holder.schedulePosition.setText(schedule.getPlace().toUpperCase());
            holder.schedulePosition.setVisibility(View.VISIBLE);
        } else {
            holder.schedulePosition.setVisibility(View.GONE);
        }

        holder.bind(position);
        final Context mContext = holder.scheduleContent.getContext();
        holder.scheduleDuration.setText(schedule.getDuration()+ " " + mContext.getResources().getString(R.string.min).toUpperCase());

        holder.trainigSighUpBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                if(mContext instanceof ScheduleActivity){
                    ((ScheduleActivity)mContext).showSignUpModal(v, schedule);
                }

            }

        });

        if (!scheduleInitialized) {

            View itemView = holder.itemView;
            itemView.setAlpha(0);

            final ObjectAnimator gridItemAlpha = ObjectAnimator.ofFloat(itemView, View.ALPHA, 0, 1);

            final ObjectAnimator gridItemTransitionY = ObjectAnimator.ofFloat(itemView, "translationY", 300f, 0f);
            gridItemTransitionY.setInterpolator(new DecelerateInterpolator());
            gridItemTransitionY.setDuration(250);
            gridItemTransitionY.setStartDelay(position * 125);

            gridItemAlpha.setDuration(250);
            gridItemAlpha.setStartDelay(position * 125);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    gridItemAlpha.start();
                    gridItemTransitionY.start();
                }
            }, 0);

        }


    }

    @Override
    public int getItemCount() {
        return scheduleList.size();
    }
}

