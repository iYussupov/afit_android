package com.fxofficeapp.a_fit.Helper;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;

import com.fxofficeapp.a_fit.Activities.ServicesActivity;
import com.fxofficeapp.a_fit.Models.Services;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Slideshow.image.SmartImageView;

import java.util.List;

/**
 * Created by Ingwar on 6/24/16.
 */
public class ServicesListAdapter extends RecyclerView.Adapter<ServicesListAdapter.ServiceViewHolder> {

    private List<Services> servicesList;

    public class ServiceViewHolder extends RecyclerView.ViewHolder {
        public CustomFont title;
        public SmartImageView thumb;

        public ServiceViewHolder(View view) {
            super(view);
            title = (CustomFont) view.findViewById(R.id.service_title);
            thumb = (SmartImageView) view.findViewById(R.id.service_thumb_bg);
        }
    }


    public ServicesListAdapter(List<Services> servicesList) {
        this.servicesList = servicesList;
    }

    @Override
    public ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_service_row, parent, false);

        return new ServiceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ServiceViewHolder holder, int position) {

        View itemView = holder.itemView;

        Services service = servicesList.get(position);
        holder.title.setText(service.getTitle().toUpperCase());
        holder.thumb.setImageUrl(service.getThumb());
        Context mContext = holder.thumb.getContext();

        if (!((ServicesActivity)mContext).viewInitialized) {

            itemView.setAlpha(0);

            final ObjectAnimator gridItemAlpha = ObjectAnimator.ofFloat(itemView, View.ALPHA, 0,1);

            final ObjectAnimator gridItemTransitionY = ObjectAnimator.ofFloat(itemView, "translationY", 300f, 0f);
            gridItemTransitionY.setInterpolator(new DecelerateInterpolator());
            gridItemTransitionY.setDuration(250);
            gridItemTransitionY.setStartDelay(position * 125);

            gridItemAlpha.setDuration(250);
            gridItemAlpha.setStartDelay(position*125);

            gridItemAlpha.start();
            gridItemTransitionY.start();

        }

    }


    @Override
    public int getItemCount() {
        return servicesList.size();
    }
}
