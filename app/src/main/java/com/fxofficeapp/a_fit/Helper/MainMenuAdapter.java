package com.fxofficeapp.a_fit.Helper;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.BaseAdapter;

import com.fxofficeapp.a_fit.Activities.MainActivity;
import com.fxofficeapp.a_fit.Models.Menu;
import com.fxofficeapp.a_fit.R;

import java.util.List;

/**
 * Created by Ingwar on 6/24/16.
 */
public class MainMenuAdapter extends BaseAdapter {
    private Context mContext;

    private List<Menu> menuList;
    private int size;
    private int lastPosition = -1;

    public MainMenuAdapter(Context c, List<Menu> menuList, int size) {
        mContext = c;
        this.menuList = menuList;
        this.size = size;
    }

    public int getCount() {
        return menuList.size();
    }

    public Object getItem(int position) {
        return menuList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public void itemAnimation(int position){

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        View grid;

        if (convertView == null) {
            grid = new View(mContext);

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.menu_gridcell, parent, false);

        } else {
            grid = (View) convertView;
        }



        com.fxofficeapp.a_fit.Helper.CustomFont title = (com.fxofficeapp.a_fit.Helper.CustomFont) grid.findViewById(R.id.menu_title);
        com.fxofficeapp.a_fit.Helper.CustomIconFont icon = (com.fxofficeapp.a_fit.Helper.CustomIconFont) grid.findViewById(R.id.menu_icon);

        Menu menu = menuList.get(position);


        String icon_name = "";
        switch (menu.getIcon()) {
            case "icon_calendar":
                icon_name = mContext.getResources().getString(R.string.icon_calendar);
                break;
            case "icon_flash":
                icon_name = mContext.getResources().getString(R.string.icon_flash);
                break;
            case "icon_camera":
                icon_name = mContext.getResources().getString(R.string.icon_camera);
                break;
            case "icon_paper":
                icon_name = mContext.getResources().getString(R.string.icon_paper);
                break;
            case "icon_personadd":
                icon_name = mContext.getResources().getString(R.string.icon_personadd);
                break;
            case "icon_email_outline":
                icon_name = mContext.getResources().getString(R.string.icon_email_outline);
                break;
            case "icon_world_outline":
                icon_name = mContext.getResources().getString(R.string.icon_world_outline);
                break;
            case "icon_clock":
                icon_name = mContext.getResources().getString(R.string.icon_clock);
                break;
        }

        title.setText(menu.getTitle().toUpperCase());
        icon.setText(icon_name);

        if (!((MainActivity)mContext).viewInitialized) {

            grid.setAlpha(0);

            ObjectAnimator gridItemTransitionY = ObjectAnimator.ofFloat(grid, "translationY", 300, 0f);
            ObjectAnimator gridItemAlpha = ObjectAnimator.ofFloat(grid, View.ALPHA, 0, 1);

            int k = -1;
            if ((position & 0x01) != 0) {
                k = 1;
            }
            ObjectAnimator gridItemTransitionX = ObjectAnimator.ofFloat(grid, "translationX", k * 100, 0f);
            gridItemTransitionX.setDuration(400);
            gridItemTransitionX.setStartDelay((position + 1) * 75);
            gridItemTransitionX.start();

            gridItemAlpha.setDuration(300);
            gridItemAlpha.setStartDelay((position + 1) * 100);
            gridItemAlpha.start();

            gridItemTransitionY.setDuration(400);
            gridItemTransitionY.setStartDelay((position + 1) * 100);
            gridItemTransitionY.setInterpolator(new OvershootInterpolator());
            gridItemTransitionY.start();

        }

        return grid;
    }

}
