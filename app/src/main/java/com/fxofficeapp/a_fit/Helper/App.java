package com.fxofficeapp.a_fit.Helper;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.fxofficeapp.a_fit.R;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.vk.sdk.VKSdk;

public class App extends MultiDexApplication {

    private static Application sApplication;

    public static Application getApplication() {
        return sApplication;
    }

    public static Context getContext() {
        return getApplication().getApplicationContext();
    }

    private void initializeVk() {
        String vkAppId = getString(R.string.vk_app_id);
        VKSdk.initialize(this, Integer.valueOf(vkAppId));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        initializeVk();
        KontaktSDK.initialize(getString(R.string.kontakt_io_api_key));
    }
}
