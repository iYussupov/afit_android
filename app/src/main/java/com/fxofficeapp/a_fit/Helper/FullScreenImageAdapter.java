package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.ImageLoader;
import com.fxofficeapp.a_fit.R;

import java.util.ArrayList;

/**
 * Created by Ingwar on 6/28/16.
 */
public class FullScreenImageAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<String> _images;
    private LayoutInflater inflater;
    private ImageLoader imageLoader;

    // constructor
    public FullScreenImageAdapter(Context c, ArrayList<String> images) {
        this._images = images;
        mContext = c;
    }

    @Override
    public int getCount() {
        return this._images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);

        String img_uri = String.valueOf(_images.get(position));
        imgDisplay.setImageUrl(img_uri);

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
