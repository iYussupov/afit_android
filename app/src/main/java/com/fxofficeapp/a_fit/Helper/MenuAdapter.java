package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.fxofficeapp.a_fit.R;

/**
 * Created by Ingwar on 5/15/16.
 */
public class MenuAdapter extends ArrayAdapter<String> {

    public MenuAdapter(Context context, String[] menuItems) {
        super(context, R.layout.custom_menu_row, menuItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater menuInflater = LayoutInflater.from(getContext());
        View customView = menuInflater.inflate(R.layout.custom_menu_row, parent, false);
        String singleMenuItem = getItem(position);
        CustomFont menuText = (CustomFont)customView.findViewById(R.id.menuItemText);

        menuText.setText(singleMenuItem);
        return customView;
    }
}
