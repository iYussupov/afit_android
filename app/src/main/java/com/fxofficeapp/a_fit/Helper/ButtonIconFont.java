package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Ingwar on 5/15/16.
 */
public class ButtonIconFont extends Button {

    public ButtonIconFont(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public ButtonIconFont(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public ButtonIconFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/ionicons.ttf", context);
        setTypeface(customFont);
    }

}
