package com.fxofficeapp.a_fit.Helper;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.BaseAdapter;

import com.android.volley.toolbox.ImageLoader;
import com.fxofficeapp.a_fit.Activities.PhotoTourActivity;
import com.fxofficeapp.a_fit.Activities.ServicesActivity;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Slideshow.image.SmartImageView;

import java.util.ArrayList;

/**
 * Created by Ingwar on 6/28/16.
 */
public class PhotoTourGridAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<String> _images;
    private int size;

    public PhotoTourGridAdapter(Context c, ArrayList<String> images, int size) {
        this._images = images;
        mContext = c;
        this.size = size;
    }

    @Override
    public int getCount() {
        return this._images.size();
    }

    @Override
    public Object getItem(int position) {
        return this._images.indexOf(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;

        if (convertView == null) {
            grid = new View(mContext);

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.phototour_gridcell, parent, false);
        } else {
            grid = (View) convertView;
        }



        SmartImageView imageView = (SmartImageView) grid.findViewById(R.id.imagepart);
        imageView.getLayoutParams().height = size;
        String img_uri = String.valueOf(_images.get(position));
        imageView.setImageUrl(img_uri);
        Context mContext = grid.getContext();

        if (!((PhotoTourActivity)mContext).viewInitialized) {

            grid.setAlpha(0);
            grid.setTranslationY(100f);

            final ObjectAnimator gridItemAlpha = ObjectAnimator.ofPropertyValuesHolder(grid,
                    PropertyValuesHolder.ofFloat("translationY", 100f, 0f)
            );

            gridItemAlpha.setDuration(250);
            gridItemAlpha.setStartDelay((position+1)*125);
            gridItemAlpha.setInterpolator(new DecelerateInterpolator());

            gridItemAlpha.start();

            final ObjectAnimator gridItemY = ObjectAnimator.ofPropertyValuesHolder(grid,
                    PropertyValuesHolder.ofFloat(View.ALPHA, 0.0f, 1.0f)
            );

            gridItemY.setDuration(500);
            gridItemY.setStartDelay((position+1)*125);
            gridItemY.setInterpolator(new DecelerateInterpolator());

            gridItemY.start();

        }

        return grid;
    }

}
