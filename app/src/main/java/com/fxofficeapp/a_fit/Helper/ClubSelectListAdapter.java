package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fxofficeapp.a_fit.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Dev1 on 12/25/16.
 */

public class ClubSelectListAdapter extends RecyclerView.Adapter<ClubSelectListAdapter.MyViewHolder> {

    Context context;
    JSONArray family;

    public class MyViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        public CustomFont familyName, familyAddress;
        public ImageView iconUnchecked, iconChecked;

        public MyViewHolder(View view) {

            super(view);
            familyName = (CustomFont) view.findViewById(R.id.family_name);
            familyAddress = (CustomFont) view.findViewById(R.id.family_address);
            iconUnchecked = (ImageView) view.findViewById(R.id.icon_unchecked);
            iconChecked = (ImageView) view.findViewById(R.id.icon_checked);
        }

        @Override
        public void onClick(View v) {

        }

    }

    public ClubSelectListAdapter(JSONArray family) {
        this.family = family;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_family_select_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        try {

            JSONArray familyData = family.getJSONArray(position);

            holder.familyName.setText(familyData.getString(0));

            if(familyData.getString(2).equals("false") || familyData.getString(2).equals("")) {
                holder.familyAddress.setVisibility(View.GONE);
            } else {
                holder.familyAddress.setVisibility(View.VISIBLE);
                holder.familyAddress.setText(familyData.getString(2));
            }

            if (Constants.URL_SLUG.equals(familyData.getString(1).replace("\\", ""))) {

                holder.iconUnchecked.setVisibility(View.GONE);
                holder.iconChecked.setVisibility(View.VISIBLE);

            } else {

                holder.iconUnchecked.setVisibility(View.VISIBLE);
                holder.iconChecked.setVisibility(View.GONE);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return family.length();
    }
}
