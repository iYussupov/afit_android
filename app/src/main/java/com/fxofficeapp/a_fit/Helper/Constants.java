package com.fxofficeapp.a_fit.Helper;

import com.fxofficeapp.a_fit.R;
import com.vk.sdk.VKScope;

import org.json.JSONArray;

/**
 * Created by Dev1 on 12/6/16.
 */

public class Constants {
    public final static String[]SCOPE = {VKScope.WALL, VKScope.PHOTOS};

    public final static String URL_BASE     = App.getContext().getString(R.string.url_base);
    public static String URL_SLUG           = App.getContext().getString(R.string.url_slug);
    public static String APP_FAMILY         = App.getContext().getString(R.string.app_family);
    public static String APP_NAME           = App.getContext().getString(R.string.app_name);
    public static String CURRENT_CLUB_NAME  = App.getContext().getString(R.string.current_club_name);

    public final static String URL_PUSH     = "push/get_archive/?orderby=date&order=desc";
    public final static String URL_POSTS    = "wp-json/wp/v2/posts";
    public final static String URL_SCHEDULE = "wp-json/wp/v2/schedule";
    public final static String URL_MAILER   = "wp-json/wp/v2/mail";
    public final static String URL_ABOUT    = "wp-json/wp/v2/aboutus";
    public final static String URL_PHOTO    = "wp-json/wp/v2/phototour";
    public final static String URL_SERVICES = "wp-json/wp/v2/services";
    public final static String URL_TRAINERS = "wp-json/wp/v2/trainers";
    public final static String URL_CONTACTS = "wp-json/wp/v2/contacts";
    public final static String URL_CLASSES  = "wp-json/wp/v2/classes";

    public final static String URL_FAMILY   = "/wp-json/wp/v2/family";

    public static JSONArray APP_FAMILY_ARRAY = new JSONArray();

    public static String DEVICE_TOKEN         = "";

    public static String TRAINING_FILTER_NAME = App.getContext().getString(R.string.all);
    public static String DEFAULT_FILTER_NAME  = App.getContext().getString(R.string.all);

}
