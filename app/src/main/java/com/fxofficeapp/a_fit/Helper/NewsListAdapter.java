package com.fxofficeapp.a_fit.Helper;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.fxofficeapp.a_fit.Activities.NewsActivity;
import com.fxofficeapp.a_fit.Models.Post;
import com.fxofficeapp.a_fit.R;
import com.fxofficeapp.a_fit.Slideshow.image.SmartImageView;

import java.util.List;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.MyViewHolder> {

    private List<Post> postList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomFont title, date, category;
        public SmartImageView thumb;

        public MyViewHolder(View view) {
            super(view);
            title = (CustomFont) view.findViewById(R.id.post_detail_title);
            date = (CustomFont) view.findViewById(R.id.post_detail_date);
            category = (CustomFont) view.findViewById(R.id.post_detail_category);
            thumb = (SmartImageView) view.findViewById(R.id.post_detail_thumb);
        }
    }


    public NewsListAdapter(List<Post> postList) {
        this.postList = postList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_news_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        View itemView = holder.itemView;
        Post post = postList.get(position);
        holder.title.setText(post.getTitle().toUpperCase());
        holder.date.setText(post.getDate());
        holder.category.setText(post.getCategory().toUpperCase());
        final Context mContext = itemView.getContext();

        final Context context = holder.thumb.getContext();
        Drawable mask = ResourcesCompat.getDrawable(context.getResources(), R.drawable.mask, null);

        String img_uri = post.getImageUrl();
        holder.thumb.setReusableHeight((Activity) context);

        if (!img_uri.isEmpty()) {
            holder.thumb.setImageUrl(img_uri);
        } else {
            holder.thumb.setImageDrawable(mask);
        }

        if (!((NewsActivity)mContext).viewInitialized) {

            itemView.setAlpha(0);

            final ObjectAnimator gridItemAlpha = ObjectAnimator.ofFloat(itemView, View.ALPHA, 0,1);

            final ObjectAnimator gridItemTransitionY = ObjectAnimator.ofFloat(itemView, "translationY", 300f, 0f);
            gridItemTransitionY.setInterpolator(new DecelerateInterpolator());
            gridItemTransitionY.setDuration(250);
            gridItemTransitionY.setStartDelay(position * 125);

            gridItemAlpha.setDuration(250);
            gridItemAlpha.setStartDelay(position*125);

            gridItemAlpha.start();
            gridItemTransitionY.start();

        }

    }


    @Override
    public int getItemCount() {
        return postList.size();
    }
}
