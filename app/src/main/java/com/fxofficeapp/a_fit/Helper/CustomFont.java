package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Ingwar on 5/15/16.
 */
public class CustomFont extends TextView {

    public CustomFont(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomFont(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public CustomFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/dinpro.ttf", context);
        setTypeface(customFont);
    }

}
