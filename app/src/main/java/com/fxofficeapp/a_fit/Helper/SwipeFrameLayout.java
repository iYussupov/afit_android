package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.fxofficeapp.a_fit.Activities.ScheduleActivity;


/**
 * Created by Ingwar on 1/14/17.
 */

public class SwipeFrameLayout extends FrameLayout {

    private boolean isBeingDragged = false;
    float initialX;
    float initialY;
    static final int MIN_DISTANCE = 200;

    public SwipeFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public SwipeFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwipeFrameLayout(Context context) {
        super(context);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        isBeingDragged = false;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            initialX = event.getX();
            initialY = event.getY();
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            isBeingDragged = false;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            float finalX = event.getX();
            float finalY = event.getY();
            float deltaX = finalX - initialX;
            float deltaY = finalY - initialY;
            if (Math.abs(deltaX) > Math.abs(deltaY)) {
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    if (deltaX > 0) {
                        ScheduleActivity.SwipeScheduleRight();
                    }
                    if (deltaX < 0) {
                        ScheduleActivity.SwipeScheduleLeft();
                    }
                }
            }
            isBeingDragged = false;
        }

        return isBeingDragged;
    }

}
