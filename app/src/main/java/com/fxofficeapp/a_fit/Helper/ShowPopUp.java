package com.fxofficeapp.a_fit.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import com.fxofficeapp.a_fit.Activities.MainActivity;
import com.fxofficeapp.a_fit.Activities.ScheduleActivity;
import com.fxofficeapp.a_fit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.fxofficeapp.a_fit.Helper.App.getContext;
import static com.fxofficeapp.a_fit.Helper.FirebaseIDService.registerToken;
import static com.fxofficeapp.a_fit.Helper.FirebaseIDService.unRegisterToken;

public class ShowPopUp {

    Context ctx;

    public ShowPopUp(Context ctx){
        this.ctx = ctx;
    }

    ArrayList<String> classItems = new ArrayList<String>();

    RecyclerView recyclerView;
    private ClubSelectListAdapter mAdapter;
    private ScheduleFilterListAdapter sAdapter;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public void clubSelect(View parent, final Activity activity){

        LayoutInflater inflater = LayoutInflater.from(ctx);
        preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        editor = preferences.edit();

        final View popupView = inflater.inflate(R.layout.fragment_club_select, null);

        final PopupWindow popupWindow = new PopupWindow(popupView,
                RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT,true);


        recyclerView = (RecyclerView)popupView.findViewById(R.id.clubFamilyRecycler);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setHasFixedSize(false);

        final JSONArray family = Constants.APP_FAMILY_ARRAY;
        // set the adapter
        mAdapter = new ClubSelectListAdapter(family);
        recyclerView.setAdapter(mAdapter);

        mAdapter.notifyDataSetChanged();

        recyclerView.addOnItemTouchListener(new CustomClickListener.RecyclerTouchListener(ctx, recyclerView, new CustomClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                try {
                    JSONArray familyData = family.getJSONArray(position);

                    if (!Constants.CURRENT_CLUB_NAME.equals(familyData.getString(0)) ) {

                        unRegisterToken(Constants.DEVICE_TOKEN);

                        editor.putString("default_app_name", familyData.getString(0));
                        editor.putString("default_app_slug", familyData.getString(1));
                        editor.commit();
                        Constants.CURRENT_CLUB_NAME = familyData.getString(0);
                        Constants.URL_SLUG = familyData.getString(1);

                        registerToken(Constants.DEVICE_TOKEN);

                        MainActivity mActivity = new MainActivity();
                        mActivity.updateClubSelectTitle();

                        mAdapter.notifyDataSetChanged();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                popupWindow.dismiss();
                            }
                        }, 400);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        com.fxofficeapp.a_fit.Helper.CustomFont cancelBtn = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.cancelBtn);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity.getWindow().setStatusBarColor(App.getContext().getResources().getColor(R.color.transparent));
                }
            }
        });

        popupWindow.setFocusable(true);

        popupWindow.setBackgroundDrawable(new ColorDrawable());

        int location[] = new int[2];
        parent.getLocationOnScreen(location);
        popupWindow.setAnimationStyle(R.style.popupanim);
        popupWindow.showAtLocation(parent, Gravity.NO_GRAVITY,
                location[0], location[1] + parent.getHeight());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(getContext().getResources().getColor(R.color.popupBgr));
        }
    }


    public void trainingsFilter(View parent, final Activity activity){

        LayoutInflater inflater = LayoutInflater.from(ctx);
        preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        editor = preferences.edit();
        classItems.add(Constants.DEFAULT_FILTER_NAME);

        final View popupView = inflater.inflate(R.layout.fragment_schedule_filter, null);

        final PopupWindow popupWindow = new PopupWindow(popupView,
                RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT,true);


        recyclerView = (RecyclerView)popupView.findViewById(R.id.scheduleFilterRecycler);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setHasFixedSize(false);

        final JSONArray family = Constants.APP_FAMILY_ARRAY;
        // set the adapter
        sAdapter = new ScheduleFilterListAdapter(classItems);
        recyclerView.setAdapter(sAdapter);

        parseClassesCacheData();

        sAdapter.notifyDataSetChanged();

        recyclerView.addOnItemTouchListener(new CustomClickListener.RecyclerTouchListener(ctx, recyclerView, new CustomClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                String className = classItems.get(position);

                Constants.TRAINING_FILTER_NAME = className;

                ScheduleActivity mActivity = new ScheduleActivity();
                mActivity.scheduleDidFiltered();

                sAdapter.notifyDataSetChanged();

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                popupWindow.dismiss();
                            }
                        }, 400);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        com.fxofficeapp.a_fit.Helper.CustomFont cancelBtn = (com.fxofficeapp.a_fit.Helper.CustomFont) popupView.findViewById(R.id.cancelBtn);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity.getWindow().setStatusBarColor(App.getContext().getResources().getColor(R.color.transparent));
                }
            }
        });

        popupWindow.setFocusable(true);

        popupWindow.setBackgroundDrawable(new ColorDrawable());

        int location[] = new int[2];
        parent.getLocationOnScreen(location);
        popupWindow.setAnimationStyle(R.style.popupanim);
        popupWindow.showAtLocation(parent, Gravity.NO_GRAVITY,
                location[0], location[1] + parent.getHeight());

    }

    void parseClassesCacheData() {

        String classes_data = preferences.getString("classes_data_"+Constants.URL_SLUG, "");
        if(!classes_data.equalsIgnoreCase(""))
        {

            Log.v("cache parse", "we are here");
            try {

                JSONObject obj = new JSONObject(classes_data);
                updateClassesModel(obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

            ScheduleActivity mActivity = new ScheduleActivity();
            mActivity.parseClassesData();

            String new_classes_data = preferences.getString("classes_data_"+Constants.URL_SLUG, "");
            if(!new_classes_data.equalsIgnoreCase("")) {
                try {
                    JSONObject classObj = new JSONObject(new_classes_data);
                    updateClassesModel(classObj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    void updateClassesModel(JSONObject response){
        try {
            classItems.removeAll(classItems);
            classItems.add(Constants.DEFAULT_FILTER_NAME);

            JSONArray classes = response.getJSONArray("classes");

            for (int i=0; i<classes.length(); i++){
                JSONObject classItemObject = new JSONObject(classes.get(i).toString());
                String classItem = classItemObject.getString("name");
                classItems.add(classItem);
            }

            sAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
