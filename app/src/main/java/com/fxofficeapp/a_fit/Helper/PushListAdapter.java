package com.fxofficeapp.a_fit.Helper;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

import com.fxofficeapp.a_fit.Activities.PushActivity;
import com.fxofficeapp.a_fit.Activities.ServicesActivity;
import com.fxofficeapp.a_fit.Models.Push;
import com.fxofficeapp.a_fit.R;

import java.util.List;

public class PushListAdapter extends RecyclerView.Adapter<PushListAdapter.MyViewHolder> {

    private List<Push> pushList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomFont date, post_id_link,message;
        public RelativeLayout divider;

        public MyViewHolder(View view) {
            super(view);
            message = (CustomFont) view.findViewById(R.id.push_message);
            date = (CustomFont) view.findViewById(R.id.push_date);
            post_id_link = (CustomFont) view.findViewById(R.id.post_id_link);
            divider = (RelativeLayout) view.findViewById(R.id.divider);
        }
    }

    public PushListAdapter(List<Push> pushList) {
        this.pushList = pushList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_push_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        View itemView = holder.itemView;
        final Push push = pushList.get(position);
        holder.message.setText(push.getMessage());
        holder.date.setText(push.getDate());

        final Context mContext = itemView.getContext();

        if (push.getPost_id() != 0) {

            holder.post_id_link.setVisibility(View.VISIBLE);

            holder.post_id_link.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (mContext instanceof PushActivity) {
                        ((PushActivity) mContext).openPostDetails(v, push.getPost_id());
                    }

                }

            });
        } else {
            holder.post_id_link.setVisibility(View.GONE);
        }

        if (position == pushList.size() - 1) {
            holder.divider.setVisibility(View.GONE);
        } else {
            holder.divider.setVisibility(View.VISIBLE);
        }

        if (!((PushActivity)mContext).viewInitialized) {

            itemView.setAlpha(0);

            final ObjectAnimator gridItemAlpha = ObjectAnimator.ofFloat(itemView, View.ALPHA, 0,1);

            final ObjectAnimator gridItemTransitionY = ObjectAnimator.ofFloat(itemView, "translationY", 300f, 0f);
            gridItemTransitionY.setInterpolator(new DecelerateInterpolator());
            gridItemTransitionY.setDuration(250);
            gridItemTransitionY.setStartDelay(position * 125);

            gridItemAlpha.setDuration(250);
            gridItemAlpha.setStartDelay(position*125);

            gridItemAlpha.start();
            gridItemTransitionY.start();

        }

    }

    @Override
    public int getItemCount() {
        return pushList.size();
    }
}
