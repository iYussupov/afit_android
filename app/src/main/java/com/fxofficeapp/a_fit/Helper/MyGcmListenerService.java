package com.fxofficeapp.a_fit.Helper;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fxofficeapp.a_fit.Activities.MainActivity;
import com.fxofficeapp.a_fit.Activities.PushActivity;
import com.fxofficeapp.a_fit.Models.Post;
import com.fxofficeapp.a_fit.R;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ingwar on 12/19/16.
 */

public class MyGcmListenerService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data){
        Log.v("DATA Received", String.valueOf(data) );
        showNotification(data);

    }

    private Post post;

    private void showNotification(Bundle data) {

        String message = data.getString("message");
        String postID = data.getString("post_id");

        Intent i = new Intent(this,MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setSound(uri)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentIntent(pendingIntent);

        final NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (postID != null) {
            // Instantiate the RequestQueue.
            final RequestQueue queue = Volley.newRequestQueue(this);
            String url = Constants.URL_BASE + Constants.URL_SLUG + Constants.URL_POSTS + "/" + postID;

            final Intent myIntent = new Intent(this, PushActivity.class);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            try {

                                JSONObject jsonData = response;

                                String category = jsonData.getString("post_category_name");
                                String postTitle = "";
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    postTitle = Html.fromHtml(jsonData.getJSONObject("title").getString("rendered"),Html.FROM_HTML_MODE_LEGACY).toString();
                                } else {
                                    postTitle = Html.fromHtml(jsonData.getJSONObject("title").getString("rendered")).toString();
                                }

                                String content = "";
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    content = Html.fromHtml(jsonData.getJSONObject("content").getString("rendered"),Html.FROM_HTML_MODE_LEGACY).toString();
                                } else {
                                    content = Html.fromHtml(jsonData.getJSONObject("content").getString("rendered")).toString();
                                }
                                String img = jsonData.getString("featured_image_thumbnail_url");
                                String date = "";

                                String dtStart = jsonData.getString("date");
                                try {

                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                    Date newDate = format.parse(dtStart);
                                    format = new SimpleDateFormat("dd.MM.yyyy");
                                    date = format.format(newDate);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                post = new Post(postTitle, date, category, img, content);

                                Bundle bundle = new Bundle();
                                bundle.putSerializable("push_received", post);
                                myIntent.putExtras(bundle);

                                PendingIntent intent2 = PendingIntent.getActivity(App.getApplication(), 1, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                                builder.setContentIntent(intent2);

                                manager.notify(0, builder.build());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
            // Add the request to the RequestQueue.
            queue.add(jsObjRequest);

        } else {
            manager.notify(0, builder.build());
        }



    }

}
