package com.fxofficeapp.a_fit.Helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Dev1 on 5/17/16.
 */
public class CustomTextViewItalic extends TextView {

    public CustomTextViewItalic(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomTextViewItalic(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public CustomTextViewItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/sfuitextitalic.otf", context);
        setTypeface(customFont);
    }

}

