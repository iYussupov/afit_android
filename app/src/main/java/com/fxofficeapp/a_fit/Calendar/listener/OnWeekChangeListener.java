package com.fxofficeapp.a_fit.Calendar.listener;

import org.joda.time.DateTime;

/**
 * Created by Ingwar on 12/29/16.
 */

public interface OnWeekChangeListener {
    void onWeekChange(DateTime firstDayOfTheWeek, boolean forward);
}