package com.fxofficeapp.a_fit.Calendar.decorator;

import android.view.View;
import android.widget.TextView;

import org.joda.time.DateTime;

/**
 * Created by Dev1 on 12/29/16.
 */

public interface DayDecorator {
    void decorate(View view, TextView dayTextView, DateTime dateTime, DateTime firstDayOfTheWeek, DateTime selectedDateTime);
}
