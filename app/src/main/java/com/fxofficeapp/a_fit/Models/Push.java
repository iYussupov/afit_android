package com.fxofficeapp.a_fit.Models;

/**
 * Created by Ingwar on 12/17/16.
 */

public class Push {

    String message, date;
    int post_id;

    public Push(String message, String date, int post_id){
        this.message = message;
        this.date = date;
        this.post_id = post_id;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }
}
