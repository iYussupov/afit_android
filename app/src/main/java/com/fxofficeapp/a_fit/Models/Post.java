package com.fxofficeapp.a_fit.Models;
import java.io.Serializable;

@SuppressWarnings("serial")
/**
 * Created by Ingwar on 5/15/16.
 */
public class Post implements Serializable {
    private String title;
    private String date;
    private String category;
    private String image_url;
    private String content;

    public Post(String title, String date, String category, String image_url, String content) {
        this.title = title;
        this.date = date;
        this.category = category;
        this.image_url = image_url;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImageUrl(){
        return image_url;
    }

    public void setImageUrl(String image_url){
        this.image_url = image_url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
