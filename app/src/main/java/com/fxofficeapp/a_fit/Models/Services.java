package com.fxofficeapp.a_fit.Models;

import java.io.Serializable;

/**
 * Created by Ingwar on 6/24/16.
 */
public class Services implements Serializable {
    private String title, content, icon, thumb;
    private int timeStamp;

    public Services(int timeStamp, String title, String content, String icon, String thumb) {
        this.timeStamp = timeStamp;
        this.title = title;
        this.content = content;
        this.icon = icon;
        this.thumb = thumb;
    }

    public int getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(int timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
