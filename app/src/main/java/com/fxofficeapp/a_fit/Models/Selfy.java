package com.fxofficeapp.a_fit.Models;

import java.io.Serializable;

/**
 * Created by Ingwar on 6/27/16.
 */
public class Selfy implements Serializable {

    private String name, image_url, tags;
    private int likes_count, comments_count;
    private boolean likes;

    public Selfy(String name, String image_url, String tags, int likes_count, int comments_count, boolean likes) {
        this.name = name;
        this.image_url = image_url;
        this.tags = tags;
        this.likes_count = likes_count;
        this.comments_count = comments_count;
        this.likes = likes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public boolean isLikes() {
        return likes;
    }

    public void setLikes(boolean likes) {
        this.likes = likes;
    }
}
