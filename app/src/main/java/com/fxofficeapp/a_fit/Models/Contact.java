package com.fxofficeapp.a_fit.Models;

import java.io.Serializable;

/**
 * Created by Dev1 on 12/9/16.
 */

public class Contact implements Serializable {
    private String title, address;
    private int timeStamp;

    private int zoom;

    private Double lng;

    private Double lat;

    private String depOneTitle;
    private String depOnePhoneOne;
    private String depOnePhoneTwo;
    private String depTwoTitle;
    private String depTwoPhoneOne;
    private String depTwoPhoneTwo;
    private String mainPhoneNumber;
    private String workDay;
    private String weekEnd;

    private String depOneEmail;
    private String depTwoEmail;

    public Contact( int timeStamp, String title, String address, Double lng, Double lat, int zoom, String depOneTitle, String depOnePhoneOne, String depOnePhoneTwo, String depTwoTitle, String depTwoPhoneOne, String depTwoPhoneTwo, String mainPhoneNumber, String workDay, String weekEnd, String depOneEmail, String depTwoEmail) {
        this.title = title;
        this.timeStamp = timeStamp;
        this.address = address;
        this.lng = lng;
        this.lat = lat;
        this.zoom = zoom;
        this.depOneTitle = depOneTitle;
        this.depOnePhoneOne = depOnePhoneOne;
        this.depOnePhoneTwo = depOnePhoneTwo;
        this.depTwoTitle = depTwoTitle;
        this.depTwoPhoneOne = depTwoPhoneOne;
        this.depTwoPhoneTwo = depTwoPhoneTwo;
        this.mainPhoneNumber = mainPhoneNumber;
        this.workDay = workDay;
        this.weekEnd = weekEnd;
        this.depOneEmail =depOneEmail;
        this.depTwoEmail = depTwoEmail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public int getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(int timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }


    public String getDepOneTitle() {
        return depOneTitle;
    }

    public void setDepOneTitle(String depOneTitle) {
        this.depOneTitle = depOneTitle;
    }

    public String getDepOnePhoneOne() {
        return depOnePhoneOne;
    }

    public void setDepOnePhoneOne(String depOnePhoneOne) {
        this.depOnePhoneOne = depOnePhoneOne;
    }

    public String getDepOnePhoneTwo() {
        return depOnePhoneTwo;
    }

    public void setDepOnePhoneTwo(String depOnePhoneTwo) {
        this.depOnePhoneTwo = depOnePhoneTwo;
    }

    public String getDepTwoTitle() {
        return depTwoTitle;
    }

    public void setDepTwoTitle(String depTwoTitle) {
        this.depTwoTitle = depTwoTitle;
    }

    public String getDepTwoPhoneOne() {
        return depTwoPhoneOne;
    }

    public void setDepTwoPhoneOne(String depTwoPhoneOne) {
        this.depTwoPhoneOne = depTwoPhoneOne;
    }

    public String getDepTwoPhoneTwo() {
        return depTwoPhoneTwo;
    }

    public void setDepTwoPhoneTwo(String depTwoPhoneTwo) {
        this.depTwoPhoneTwo = depTwoPhoneTwo;
    }

    public String getMainPhoneNumber() {
        return mainPhoneNumber;
    }

    public void setMainPhoneNumber(String mainPhoneNumber) {
        this.mainPhoneNumber = mainPhoneNumber;
    }

    public String getWorkDay() {
        return workDay;
    }

    public void setWorkDay(String workDay) {
        this.workDay = workDay;
    }

    public String getWeekEnd() {
        return weekEnd;
    }

    public void setWeekEnd(String weekEnd) {
        this.weekEnd = weekEnd;
    }

    public String getDepOneEmail() {
        return depOneEmail;
    }

    public void setDepOneEmail(String depOneEmail) {
        this.depOneEmail = depOneEmail;
    }

    public String getDepTwoEmail() {
        return depTwoEmail;
    }

    public void setDepTwoEmail(String depTwoEmail) {
        this.depTwoEmail = depTwoEmail;
    }
}
