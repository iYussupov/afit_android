package com.fxofficeapp.a_fit.Models;

import java.io.Serializable;

/**
 * Created by Dev1 on 11/28/16.
 */

public class Menu implements Serializable {
    private String title;
    private String icon;

    public Menu(String title, String icon) {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
