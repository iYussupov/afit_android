package com.fxofficeapp.a_fit.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Dev1 on 12/9/16.
 */

public class About implements Serializable {
    private String title, content;
    private int timeStamp;
    private JSONObject image_url;

    public About( int timeStamp, String title, String content, JSONObject image_url) {
        this.title = title;
        this.timeStamp = timeStamp;
        this.image_url = image_url;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public int getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(int timeStamp) {
        this.timeStamp = timeStamp;
    }

    public JSONObject getImageUrl(){
        return image_url;
    }

    public void setImageUrl(JSONObject image_url){
        this.image_url = image_url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}