package com.fxofficeapp.a_fit.Models;

import java.io.Serializable;

/**
 * Created by Ingwar on 6/28/16.
 */
public class Schedule implements Serializable {
    private String title, content, place, start_time, start_date, end_date, duration;
    int weekday, timeStamp;

    public Schedule(int timeStamp, int weekday, String title, String content, String place, String start_time, String start_date, String end_date, String duration) {
        this.title = title;
        this.content = content;
        this.place = place;
        this.start_time = start_time;
        this.start_date = start_date;
        this.end_date = end_date;
        this.duration = duration;
        this.weekday = weekday;
        this.timeStamp = timeStamp;
    }

    public int getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(int timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getWeekday() {
        return weekday;
    }

    public void setWeekday(int weekday) {
        this.weekday = weekday;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getStartTime() {
        return start_time;
    }

    public void setStartTime(String start_time) {
        this.start_time = start_time;
    }

    public String getStartDate() {
        return start_date;
    }

    public void setStartDate(String start_date) {
        this.start_date = start_date;
    }

    public String getEndDate() {
        return end_date;
    }

    public void setEndDate(String end_date) {
        this.end_date = end_date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
