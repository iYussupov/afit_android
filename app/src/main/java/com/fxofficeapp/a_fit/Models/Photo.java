package com.fxofficeapp.a_fit.Models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Dev1 on 12/9/16.
 */

public class Photo implements Serializable {
    private int timeStamp;
    private JSONObject image_url;

    public Photo( int timeStamp, JSONObject image_url) {
        this.timeStamp = timeStamp;
        this.image_url = image_url;
    }


    public int getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(int timeStamp) {
        this.timeStamp = timeStamp;
    }

    public JSONObject getImageUrl(){
        return image_url;
    }

    public void setImageUrl(JSONObject image_url){
        this.image_url = image_url;
    }

}
