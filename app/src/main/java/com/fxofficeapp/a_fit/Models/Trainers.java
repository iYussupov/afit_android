package com.fxofficeapp.a_fit.Models;

import java.io.Serializable;

/**
 * Created by Ingwar on 6/24/16.
 */
public class Trainers implements Serializable {
    private String name, featuredImg, category, content;
    private int timeStamp;

    public Trainers(int timeStamp, String name, String featuredImg, String category, String content) {
        this.timeStamp = timeStamp;
        this.name = name;
        this.featuredImg = featuredImg;
        this.category = category;
        this.content = content;
    }

    public int getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(int timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFeaturedImg() {
        return featuredImg;
    }

    public void setFeaturedImg(String featuredImg) {
        this.featuredImg = featuredImg;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
